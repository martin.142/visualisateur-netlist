# 29 / 11 / 2021
# Mobj cours


## VII.6 La fenetre principale

QMainWindow
- QWidgetFlotant : sousfenetre

```c++

#include 

class CellViewer : public QMainWindow {
    Q_OBJECT:
public:
          CellViewer ( QWidget* parent=NULL );
    void  setCell    (Cell* );
    Cell* getCell    () const;
public slots:
    void saveCell ();
private:
    CellWidget*     CellWidget_;
    SaveCellDialog* SaveCellDialog_;
};


CellViewer::CellViewer ( QWidget* parent)
: QMainWindow    (parent)
, CellWidget_    (NULL)
, SaveCellDialog_(NULL)
{
    CellWidget_
    SaveCellDialog_ = 

    QMenu* fileMenu = menuBar()->addMenu("&File");

    QAction* action = new QAction ("&Save As", this);
    action->setStatusTip ("Save to disk (rename) the Cell");
    action->setShortcut (QKeySequence("CTRL-S"));
    action->setVisible(true);
    filemenu->addAction(action);
    connect(action, SIGNAL(triggered()), this, SLOT(saveCell()));

    action = new QAction("&Quit", this);
    action->setStatusTip("Exit the Netlist Viewer");
    action->setShortcut(QKeySequence("CTRL-Q"));
    action->setVisible(true);
    fileMenu->addAction(action);
    connect(action, SIGNAL(triggered()), this, SLOT(close()));
}

CellViewer::saveCell()
{
    Cell* cell = getCell();
    if(cell = NULL) return;

    QString cellName = cell->getName().c_str();

    if(SaveCellDialog_->run(cellName)){
        cell->setName(cellName.toStdString());
        cell->save   (cellName.toStdString());
    }
}

```

```c++
#include <QApplication>
#include <QtGui>
#include "CellViewer.h"

int main(int argc, char* argv[]){
    QApplication* qa = new QApplication(argc, argv);

    CellViewer* viewer = new CellViewer();
    viewer->setCell(halfadder);
    viewer->show();

    int rvalue = qa->exec();
    delete qa;
    return rvalue;
}

```

```c++

#include <QAbstractTableModel>

class InstanceModel : public QAbstractTableModel {
    Q_OBJECT;
public:
                InstanceModel   (Q_Object* parent = NULL);
                ~InstanceModel  ();
    void        setCell         (Cell*);
    Cell*       getModel        (int row),
    int         rowCount        (const QModelIndex& parent = QModelIndex());
    int         columnCount     (const QModelIndex& parent = QModelIndex());
    QVariant    data            (const QModelIndex& index, int role = Qt::displayRole;
    QVariant    headerData      (int section, Qt::Orientation orientation, int role= Qt::displayRole) const;
private:
    Cell* cell_;
};

/*
    QModelIndex = A quel cass one s'adresse
        - Row()
        - Column()
        - ADresse dans le tableau
*/

InstanceModel::InstanceModel(Q_Object* parent)
:QAbstractTableModel(parent)
, cell_(NULL);
{}

InstanceModel::~InstanceModel()
{ }

void InstanceModel::setCell(Cell* cell){
    emit layoutAboutToBeChanged();  //Stop refreshing the view
    cell_ = cell;
    emit layoutChanged(); //Restart showing data
}

int InstanceModel::rowCount(cont QModelIndex& parent) const {
    return (Cell_) ? cell_->getInstances().size() : 0;
} 

int InstanceModel::columnCount(const QModelIndex& parent) const{
    return 2;
}

QVariant InstanceModel::data(const QModelIndex& index, int role) const {
    if(not cell_ or not idnex.isValid()) return QVariant();
    if(role == QT::displayRole){
        int row = index.row();
        switch( index.column()){
            case 0: return cell_->getInstances()[row]->getName.c_str();
            case 1: return cell_->getInstances()[row]->getMasterCell()->getName().c_str();
        }
    }
    return QVariant();
}

//  QVariant permet de retourne n'importe quel type de donné de base

QVariant InstanceModel::headerData(int section, Qt::Orientation orientation, int role) const{
    if(orientation == Qt::Vertical) return QVariant();
    if(role != Qt::displayRole) return QVariant();

    switch (section){
        case 0: return "Instance";
        case 1: return "MasterCell";
    }
    return QVariant();
}

Cell* InstanceModel::getModel(int row){
    if(not Cell_)
}


```





```c++

class InstanceWidget : public QWidget {

};

InstanceWidget::InstanceWidget (QWidget* parent)
: QWidget(parent)
, CellViewer(NULL)
, baseModel_ (new InstanceModel(this))
, view_ (new QTableView(this))
, laod_ (new QPushButton(this))
{
    setAttribut(QT::WA_QuitOnClose, false);
    setAttribut(Qt::WA_DeleteOnClose, false);
    setContextMenuPolicy(Qt::ActionsContextMenu);

    view_->setShowGrid(false);
    view_->
}

InstanceWidget::Instances

```


### Exceptions

Une exeption écrase la pile, toutes les fonctions qui étaient pendant pour finir dans la pile, ne seront pas termines. Le grand probleme c'est que les vairiables allouers dynamiquement ne seront jamais supprimes, donc cela provoque des problemes de fuite de memoires.

Par défaut, une execption d'echape de toutes les fonction qui étaient dans la pile. Si l'exeption s'echape du main, le programme s'arrete. 

try(){
    function();
} 
catch (std::bad_alloc e){
    cerr << e.what() << endl;
} 
catch (...){ // Catch all exeptions.
    cerr << e.what() << endl;
} 




# 6 - 12 - 2021
    

La cordonnée d'une terminal d'une instance est ataché directement dans le TermNode du Term de l'instance. Dans Instance::setPosition(), non seulement on met à jur les cordonnées de l'instance mas aussi celle de tous ses termianlles à partir du synmbol. On applique la traslation de l'instance une seule fois.