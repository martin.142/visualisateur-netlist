/**
 * @file Net.cpp
 * @author ACOSTA CORRAL Martin Eduardo (martin.acostaec@gmail.com)
 * @author BOUHMID Zakaria              (zakaria.bmd@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2022-01-02
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include  <libxml/xmlreader.h>
#include  <libxml/xmlstring.h>
#include  <vector>
#include <iostream>
#include  "Net.h"
#include  "Term.h"
#include  "Instance.h"
#include  "Cell.h"
#include "XmlUtil.h"

namespace Netlist{

	Net::Net (Cell* owner, std::string name, Term::Type type)
		:owner_(owner), 
		 name_(name), 
		 type_(type)
	{
		owner_->add(this);
	}

	Net::~Net(){
		for(auto n = nodes_.begin(); n != nodes_.end(); n++){
			nodes_.erase( n );
		}
	}

	/**
	 * @brief Get a free node id for a new node connection
	 * 
	 * @return size_t Number of the free node.
	 */
	size_t Net::getFreeNodeId() const{ 
		
		for(size_t i = 0; i < nodes_.size(); i++){
			if(nodes_[i] == NULL)
				return i;
		}
		return nodes_.size();
	}
	
	/**
	 * @brief Add a node to the vector of nodes.
	 * If node null, finish function
	 * If id of the node not assigned, get a new id
	 * if node id is bigger than the size of the vector, add NULL spaces until 
	 * 		id position
	 * 
	 * @param node 
	 */
	void Net::add(Node* node){
		if(not node) 
      return;

    //FIND ID AVAILABLE
		size_t id = node->getId();
		if(id==Node::noid){
			id = getFreeNodeId();
			node->setId(id);
		}

		if(id < nodes_.size()){		//If enough space, affect the position id
			if(nodes_[id] == NULL){
        nodes_[id] = node;
			}
		}else{										// if not enough space, push back NULL elements until position id
			for(size_t i=nodes_.size(); i<id; i++){
				nodes_.push_back(NULL);
			}
			nodes_.push_back(node);
		}
	}

	/**
	 * @brief  Remove a node from vector of nodes
	 * 
	 * @param node Node to be removed
	 * @return true 	Node removed	
	 * @return false 	Node not present
	 */
	bool Net::remove(Node* node){
		for ( std::vector<Node*>::iterator inode=nodes_.begin() ; inode != nodes_.end() ; ++inode ) {
      	if (*inode == node) nodes_.erase( inode );
      	return true;
    }
    return false;
	}

  /*
   * ================================================================================================================
   * ================================================================================================================
   * ======================================                          ================================================
   * ====================================== READ AND WRITE XML FILES ================================================
   * ======================================                          ================================================
   * ================================================================================================================
   * ================================================================================================================
   */


	Net* Net::fromXml(Cell* cell, xmlTextReaderPtr & reader){		
    std::string 		NetName     = xmlCharToString(xmlTextReaderGetAttribute( reader, (const xmlChar*)"name" ) );
    std::string 		NetTypeStr  = xmlCharToString(xmlTextReaderGetAttribute( reader, (const xmlChar*)"type" ) );
    Term::Type 			NetType     = (NetTypeStr == "Internal")?Term::Internal:Term::External;

    if(NetName.empty() or NetTypeStr.empty()){
      return NULL;
    } else {
      NetType = (NetTypeStr == "Internal")?Term::Internal:Term::External;
      return new Net(cell, NetName, NetType); 
    }
	}


	/**
	 * @brief add a line to vector of lines
	 * 	
	 * @param line Line to be added
	 */
	void Net::add ( Line* line ){
		if (line) lines_.push_back( line );
	}

	/**
	 * @brief Remove a line from vector of lines
	 * 
	 * @param line 		Line to be removed
	 * @return true		Line removed correclty
	 * @return false 	Line not found
	 */
	bool  Net::remove ( Line* line )
	{
		if (line) {
			for ( std::vector<Line*>::iterator il = lines_.begin()
					; il != lines_.end() ; ++il ) {
				if (*il == line) {
					lines_.erase( il );
					return true;
				}
			}
		}
		return false;
	}

} // Netlist Namespace