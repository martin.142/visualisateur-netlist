/**
 * @file OpenCellDialog.h
 * @author ACOSTA CORRAL Martin Eduardo (martin.acostaec@gmail.com)
 * @author BOUHMID Zakaria              (zakaria.bmd@gmail.com)
 * @brief QT dialog to open a new cell
 * @version 0.1
 * @date 2022-01-02
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef NETLIST_OPEN_CELL_DIALOG_H
#define NETLIST_OPEN_CELL_DIALOG_H

#include <QDialog>
#include <QWidget>
#include <QLineEdit>

namespace Netlist {

  class CellWidget;
  class CellsLib;
  class InstancesWidget;

  class OpenCellDialog : public QDialog {
      Q_OBJECT;
    public:
                        OpenCellDialog  ( QWidget* parent=NULL );
              bool      run             ( QString & name );
      const   QString   getCellName     () const;
              void      setCellName     ( const QString & );

    protected:
      QLineEdit * lineEdit_ ;

    private:
      CellWidget*      cellWidget_;
      CellsLib*        cellsLib_;         // TME9+.
      InstancesWidget* instancesWidget_;  // TME9+.
      OpenCellDialog*  openCellDialog_;
  };
}

#endif //NETLIST_OPEN_CELL_DIALOG_H