/**
 * @file Term.cpp
 * @author ACOSTA CORRAL Martin Eduardo (martin.acostaec@gmail.com)
 * @author BOUHMID Zakaria              (zakaria.bmd@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2022-01-02
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include  <libxml/xmlreader.h>
#include  <vector>
#include <cstdio>
#include "Term.h"
#include "Cell.h"
#include "Net.h"
#include "Node.h"
#include "XmlUtil.h"

namespace Netlist{

  /**
   * @brief Construct a new Term:: Term object
   * This term belogns to a cell, so it is external
   * 
   * @param owner   Cell owner
   * @param name    Name identificator
   * @param dir     Direction of term (internal or external)
   */
	Term::Term(Cell* owner, std::string name, Direction dir)
		:owner_(owner), 
		name_(name), 
		direction_(dir),
    type_(External), //External type when Cell as an argument
    net_(NULL),
    node_(this),
    position_()
	{
		owner->add(this); //Add Terms to vector of terms in owner cell
	}

  /**
   * @brief Construct a new Term:: Term object from modelTerm
   * Data is copied from modelTem to the new term in construction
   * This term belongs to a instance, so it is internal
   * 
   * 
   * @param owner     Owner instance
   * @param modelTerm Term to copy data from
   */
	Term::Term(Instance* owner, const Term* modelTerm)
		:owner_(owner),
    type_(Internal),
    net_(NULL),
    node_(this)
	{
    name_       = modelTerm->getName();
    direction_  = modelTerm->getDirection();
    position_   = modelTerm->getPosition();
    node_.setPosition(getPosition());
	}

  Term::~Term()
  {}

  /**
   * @brief Link a net to this term
   * 
   * @param net  Pointer to net to be linked
   */
  void Term::setNet(Net* net){
  	net_ = net;
  	node_.setId(net_->getFreeNodeId());
  	net_->add(&node_);
  }

  /**
   * @brief OVERLOADED FUNCTION
   * Link a net to this term by locking the net by name
   * 
   * @param name Name of the net to look for
   */
  void Term::setNet(std::string name){
  	setNet(getCell()->getNet(name));
  }

  void Term::setPosition(const Point point){
    position_ = point;
  }

  void Term::setPosition(int x, int y){
    setPosition(Point(x, y));
  }

  /*
   * To string fucntions
   * Direction data type has a number asociated to every element, 
   * they return the element from an array of string with the asociated number
   * as an index
   */  

  /**
   * @brief To string functions
   * Direction data type has a number asociated to every element
   * they return the element from an array of string with the asociated number
   * as an index
   * 
   * @param dir 
   * @return std::string 
   */
  std::string Term::toString(Direction dir){
  	std::string tab[6] = {"In","Out", "Inout", "Tristate", "Transcv", "Unknown"};
  	return tab[dir-1];
  }

  std::string Term::toString(Type type){
  	std::string tab[6] = {"Internal","External"};
  	return tab[type-1];
  }


  /**
   * @brief Compares a string with the name of Directions as String, it return the index
   * or 6 (unknow) if not found
   * 
   * @param str 
   * @return Term::Direction 
   */
  Term::Direction Term::toDirection(std::string str){
  	std::string tab[6] = {"In","Out", "Inout", "Tristate", "Transcv", "Unknown"};
  	for(int i = 1; i<=6; i++){
  		if(!tab[i-1].compare(str)){
        return (Term::Direction)i; //Cast constant integer to Direction enum number
  		}
  	}
  	return Term::Unknown; //Cast constant integer to Direction enum number
  }


   /*
   * ================================================================================================================
   * ================================================================================================================
   * ======================================                          ================================================
   * ====================================== READ AND WRITE XML FILES ================================================
   * ======================================                          ================================================
   * ================================================================================================================
   * ================================================================================================================
   */


  /**
   * @brief Write to ostream the XML data of current term
   * 
   * @param stream 
   */
  void Term::toXml(std::ostream& stream){
    stream  << indent << "<term name=\"" << getName() 										 << "\"" 
  										<< " direction=\"" << Term::toString(getDirection()) << "\""
                      << " x=\""         << getPosition().getX()           << "\""
                      << " y=\""         << getPosition().getY()           << "\"/>\n";
  }

  /**
   * @brief Read XML data for curent XML reader line
   * 
   * @param cell    Current CELL
   * @param reader  Reader
   * @return Term*  Term created, NULL if Name or Direction not found
   */
  Term* Term::fromXml(Cell* cell, xmlTextReaderPtr reader){
    //Parameters to read
    std::string TermName  =             xmlCharToString(xmlTextReaderGetAttribute( reader, (const xmlChar*)"name" ) );
    Direction   dir       = toDirection(xmlCharToString(xmlTextReaderGetAttribute( reader, (const xmlChar*)"direction")));
    int         x         = std::stoi(  xmlCharToString(xmlTextReaderGetAttribute( reader, (const xmlChar*)"x")));
    int         y         = std::stoi(  xmlCharToString(xmlTextReaderGetAttribute( reader, (const xmlChar*)"y")));

    //If Name or direction not found, return NULL
    if(TermName.empty() or dir == Unknown  )
      return NULL;

    //Create new term, set position readed, and return  term
    Term* term = new Term(cell, TermName, dir);
    term->setPosition(x,y);
    return term;
  }

} // Namespace NetList