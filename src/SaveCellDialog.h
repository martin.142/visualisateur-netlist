/**
 * @file SaveCellDialog.h
 * @author ACOSTA CORRAL Martin Eduardo (martin.acostaec@gmail.com)
 * @author BOUHMID Zakaria              (zakaria.bmd@gmail.com)
 * @brief QT dialog window
 * Save the current cell with a new name
 * @version 0.1
 * @date 2022-01-02
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef NETLIST_SAVE_CELL_DIALOG_H
#define NETLIST_SAVE_CELL_DIALOG_H

#include <QDialog>
#include <QWidget>
#include <QLineEdit>

namespace Netlist {

  class CellWidget;
  class CellsLib;
  class InstancesWidget;

  class SaveCellDialog : public QDialog {
      Q_OBJECT;
    public:
                        SaveCellDialog  ( QWidget* parent=NULL );
              bool      run             ( QString & name );
      const   QString   getCellName     () const;
              void      setCellName     ( const QString & );

    protected:
      QLineEdit * lineEdit_ ;

    private:
      CellWidget*      cellWidget_;
      CellsLib*        cellsLib_;        
      InstancesWidget* instancesWidget_; 
      SaveCellDialog*  saveCellDialog_;
  };
}

#endif //NETLIST_SAVE_CELL_DIALOG_H