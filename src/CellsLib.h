/**
 * @file CellsLib.h
 * @author ACOSTA CORRAL Martin Eduardo (martin.acostaec@gmail.com)
 * @author BOUHMID Zakaria              (zakaria.bmd@gmail.com)
 * @brief window for list of cells
 * @version 0.1
 * @date 2022-01-02
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef NETLIST_CELLS_LIB_H
#define NETLIST_CELLS_LIB_H

#include <QTableView>
#include <QPushButton>
#include  <QTableView>
#include <QPushButton>
#include <iostream>
#include "CellViewer.h"
#include "CellsModel.h"


namespace Netlist{

  class Cell;

  class CellsLib : public QWidget {
    Q_OBJECT;
  public:
                      CellsLib       ( QWidget* parent=NULL );
          virtual     ~CellsLib ();
          void        setCellViewer  ( CellViewer* );
          int         getSelectedRow () const;
          CellsModel* getBaseModel   ();

  public slots:
           void        load           ();

  private:
    CellViewer*  cellViewer_;
    CellsModel*  baseModel_;
    QTableView*  view_;
    QPushButton* load_;
};

}// Namespace Netlist

#endif