/**
 * @file Symbol.cpp
 * @author ACOSTA CORRAL Martin Eduardo (martin.acostaec@gmail.com)
 * @author BOUHMID Zakaria              (zakaria.bmd@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2022-01-02
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include  <libxml/xmlreader.h>
#include  <string>
#include  <vector>
#include  "Symbol.h"
#include  "Box.h"
#include  "Cell.h"
#include  "Term.h"
#include  "Shape.h"



namespace Netlist {

  class Cell;

  Symbol::Symbol (Cell* owner) : owner_(owner) 
  {}

  Symbol::Symbol(const Symbol& symbol)
  {
    owner_ = symbol.getCell();
  }

  /**
   * @brief Destroy the Symbol:: Symbol object
   * Destroy element from vector of shapes
   * 
   */
  Symbol::~Symbol(){
    while ( not shapes_.empty() ) delete *shapes_.begin();
  }

  /**
   * @brief Operator = to copy a symbol
   * 
   * @param symbSrc   Symbol to be copied
   * @return Symbol&  Symbol copied
   */
  Symbol& Symbol::operator=(const Symbol& symbSrc){
    //=================================================== VERIFY IF CORRECT
    Symbol* sTemp = new Symbol(symbSrc.getCell());  // Copy Cell to the new symbol
    for(auto* s : symbSrc.getShapes()){             // Copy all shapes to the new symbol
      sTemp->add(s);
    }
    return *sTemp;
  }

  /**
   * @brief Bounding box calulate and create the box object corresponding to the current cell
   * To create, the box, it take the position of every instance, then adds the position of each
   * point to be drawed. The objectif is to find the farthest distance for evey axis so
   * it can creat a box enough large to contain al objects in the cell.  
   * 
   * @return Box Object box of the cell
   */
  Box Symbol::getBoundingBox () const {
    
    int x1 = 99999, x2  = 0, y1  = 9999, y2  = 0;


    if(owner_->getInstances().size() != 0){
      
      //Calculate x1 & x2 from terms positions
      for(auto *t : owner_->getTerms()){
        if(t->getPosition().getX() < x1) x1 = t->getPosition().getX();
        if(x2 < t->getPosition().getX()) x2 = t->getPosition().getX(); 
      }
      
      //Calculate y1 & y2 from instances positions & its size
      for(auto *i : owner_->getInstances()){ 
        for(auto *s : i->getMasterCell()->getSymbol()->getShapes()){
          BoxShape      * boxS      = dynamic_cast<BoxShape*>(s);
          LineShape     * lineS     = dynamic_cast<LineShape*>(s);
          ArcShape      * arcS      = dynamic_cast<ArcShape*>(s);
          EllipseShape  * ellipseS  = dynamic_cast<EllipseShape*>(s);
          if(boxS){
            if(boxS->getY1() + i->getPosition().getY() < y1) y1 = boxS->getY1() + i->getPosition().getY();
            if(y2 < boxS->getY2() + i->getPosition().getY()) y2 = boxS->getY2() + i->getPosition().getY();
          }
          if(lineS){
            if(lineS->getY1() + i->getPosition().getY() < y1) y1 = lineS->getY1() + i->getPosition().getY();
            if(lineS->getY2() + i->getPosition().getY() < y1) y1 = lineS->getY2() + i->getPosition().getY();
            if(y2 < lineS->getY2() + i->getPosition().getY()) y2 = lineS->getY2() + i->getPosition().getY();
          }
          if(arcS){
            if(arcS->getY1() + i->getPosition().getY() < y1) y1 = arcS->getY1() + i->getPosition().getY();
            if(y2 < arcS->getY2() + i->getPosition().getY()) y2 = arcS->getY2() + i->getPosition().getY();
          }
          if(ellipseS){
            if(ellipseS->getY1() + i->getPosition().getY() < y1) y1 = ellipseS->getY1() + i->getPosition().getY();
            if(y2 < ellipseS->getY2() + i->getPosition().getY()) y2 = ellipseS->getY2() + i->getPosition().getY();
          }
        }
      }
    } else {
      for(auto *s : owner_->getSymbol()->getShapes()){
        BoxShape      * boxS      = dynamic_cast<BoxShape*>(s);
        LineShape     * lineS     = dynamic_cast<LineShape*>(s);
        ArcShape      * arcS      = dynamic_cast<ArcShape*>(s);
        EllipseShape  * ellipseS  = dynamic_cast<EllipseShape*>(s);
        if(boxS){
          if(boxS->getY1() < y1) y1 = boxS->getY1();
          if(y2 < boxS->getY2() ) y2 = boxS->getY2();

          if(boxS->getX1() < x1) x1 = boxS->getX1();
          if(x2 < boxS->getX2()) x2 = boxS->getX2();
        }
        if(lineS){
          if(lineS->getY1() < y1) y1 = lineS->getY1();
          if(lineS->getY2() < y1) y1 = lineS->getY2();
          if(y2 < lineS->getY2()) y2 = lineS->getY2();

          if(lineS->getX1() < x1) x1 = lineS->getX1();
          if(lineS->getX2() < x1) x1 = lineS->getX2();
          if(x2 < lineS->getX2()) x2 = lineS->getX2();
        }
        if(arcS){
          if(arcS->getY1() < y1) y1 = arcS->getY1();
          if(y2 < arcS->getY2() ) y2 = arcS->getY2();

          if(arcS->getX1() < x1) x1 = arcS->getX1();
          if(x2 < arcS->getX2()) x2 = arcS->getX2();
        }
        if(ellipseS){
          if(ellipseS->getY1() < y1) y1 = ellipseS->getY1();

          if(ellipseS->getX1() < x1) x1 = ellipseS->getX1();
          if(x2 < ellipseS->getX2()) x2 = ellipseS->getX2();
        }
      }
    }
    y1 -= 10;
    y2 += 10;

    Box sideBox(x1,y1,x2,y2); 

    return sideBox;
  }

  /**
   * @brief 
   * 
   * @param term 
   * @return Point 
   */
  Point Symbol::getTermPosition (Term* term) const {
    return term->getPosition();
  }

  /**
   * @brief 
   * 
   * @param term 
   * @return TermShape* 
   */
  TermShape* Symbol::getTermShape(Term* term) const {
    //return owner_->getTerm(); 
    return NULL;
  }

  void Symbol::add(Shape* shape){
    shapes_.emplace_back(shape);
  }

  void Symbol::remove(Shape* shape){
    for(std::vector<Shape*>::iterator it = shapes_.begin(); it != shapes_.end(); ++it){
      if(*it == shape){
        shapes_.erase(it);
      }
    }
  }




  /*
   * ================================================================================================================
   * ================================================================================================================
   * ======================================                          ================================================
   * ====================================== READ AND WRITE XML FILES ================================================
   * ======================================                          ================================================
   * ================================================================================================================
   * ================================================================================================================
   */




  void Symbol::toXml (std::ostream& stream) const {
    stream << indent++ << "<symbol>\n";
    for(auto* s : shapes_){
      s->toXml(stream);
    }
    stream << --indent << "</symbol>\n";
  }

  Symbol* Symbol::fromXml(Cell* owner, xmlTextReaderPtr reader){
    Shape* s = Shape::fromXml(owner->getSymbol(), reader);
    return s->getSymbol();
  }
  


} // namespace netlist