/**
 * @file InstancesWidget.cpp
 * @author ACOSTA CORRAL Martin Eduardo (martin.acostaec@gmail.com)
 * @author BOUHMID Zakaria              (zakaria.bmd@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2022-01-02
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <QWidget>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QString>
#include <QHeaderView>
#include <iostream>
#include  "Cell.h"
#include "InstancesModel.h"
#include "InstancesWidget.h"


namespace Netlist {

  using namespace std;

  InstancesWidget::InstancesWidget ( QWidget* parent )
    : QWidget(parent)
    , cellViewer_  ()
    , baseModel_ (new InstancesModel(this))
    , view_ (new QTableView(this))
    , load_ (new QPushButton(this))
  {
    
    setAttribute( Qt::WA_QuitOnClose , false );
    setAttribute( Qt::WA_DeleteOnClose , false );     // Close only instances window
    setContextMenuPolicy( Qt:: ActionsContextMenu );
    
    /**
     * @brief Table attributes and data
     * Data is reading by seting the base Model as model.
     * base model has the algorithm to read data
     */
    view_ ->setShowGrid ( false );
    view_ ->setAlternatingRowColors( true );
    view_ ->setSelectionBehavior ( QAbstractItemView::SelectRows );
    view_ ->setSelectionMode ( QAbstractItemView::SingleSelection );
    view_ ->setSortingEnabled ( true );
    view_ ->setModel ( baseModel_ ); // On associe le model

    /**
     * @brief Header of table. 
     * Align text to center
     * minimum size of 200
     */
    QHeaderView* horizontalHeader = view_ ->horizontalHeader ();
    horizontalHeader ->setDefaultAlignment ( Qt::AlignHCenter );
    horizontalHeader ->setMinimumSectionSize( 200 );
    horizontalHeader ->setStretchLastSection( true );

    QHeaderView* verticalHeader = view_ ->verticalHeader ();
    verticalHeader ->setVisible( false );

    /**
     * @brief Horizontal layout 
     * Strech = |
     * | load button |
     */
    QHBoxLayout* hLayout = new QHBoxLayout ();
    hLayout->addStretch();
    hLayout->addWidget( load_ );
    hLayout->addStretch();

    /**
     * @brief vertival layout 
     *  TABLE OF INSTANCES  
     *  HORIZONTAL LAYOUT
     */
    QVBoxLayout* vLayout = new QVBoxLayout ();
    vLayout->addWidget( view_ );
    vLayout->addLayout( hLayout );
    setMinimumWidth(400);
    setLayout ( vLayout );

    load_ ->setText( "Load" );
    connect( load_, SIGNAL(clicked ()), this, SLOT(load ()) );

    //Detect Double click on table view and call load()
    connect (view_, SIGNAL(doubleClicked(const QModelIndex &)), this, SLOT(load()));
  }

  InstancesWidget::~InstancesWidget ()
  { }

  /**
   * @brief Load cell selecte from table of instances
   * 
   */
  void InstancesWidget::load()
  {
    int selectedRow = getSelectedRow ();
    if (selectedRow < 0) return;
    cellViewer_->setCell( baseModel_->getModel(selectedRow) );
  }

  /**
   * @brief Read selected row
   * 
   * @return int Selected row index
   */
  int InstancesWidget::getSelectedRow () const
  {
    QModelIndexList selecteds = view_->selectionModel()->selection().indexes ();
    if (selecteds.empty ()) return -1;
    return selecteds.first().row();
  }

  /**
   * @brief Set a new cell viewer
   * 
   * @param cellViewer 
   */
  void InstancesWidget::setCellViewer( CellViewer* cellViewer)
  {
    cellViewer_ = cellViewer;
  }


  /**
   * @brief set call. Functionusally called by cell viewer
   * 
   * @param cell 
   */
  void  InstancesWidget::setCell ( Cell* cell )
  {
    baseModel_->setCell(cell);
    repaint();
  }

}  // Netlist namespace.