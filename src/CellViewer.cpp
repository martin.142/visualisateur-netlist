/**
 * @file CellViewer.cpp
 * @author ACOSTA CORRAL Martin Eduardo (martin.acostaec@gmail.com)
 * @author BOUHMID Zakaria              (zakaria.bmd@gmail.com)
 * @brief Cell Viewer is the main window
 * @version 0.1
 * @date 2022-01-02
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include  <QResizeEvent>
#include  <QPainter>
#include  <QPen>
#include  <QBrush>
#include  <QFont>
#include  <QApplication>
#include  <QMenuBar>
#include <iostream>
#include "Cell.h"
#include "CellViewer.h"
#include "CellWidget.h"
#include "SaveCellDialog.h"
#include "OpenCellDialog.h"
#include "InstancesWidget.h"
#include "CellsLib.h"
#include "Net.h"

namespace Netlist {
  CellViewer::CellViewer ( QWidget* parent): 
    
  /**
   * @brief Construct a new QMainWindow object
   * 
   */
  QMainWindow(parent), cellWidget_(NULL), cellsLib_(NULL), instancesWidget_(NULL), saveCellDialog_(NULL), openCellDialog_(NULL)  {
    
    /**
     * @brief Initialize object of the window
     * 
     */
    cellWidget_       = new CellWidget(parent);
    saveCellDialog_   = new SaveCellDialog( this );
    openCellDialog_   = new OpenCellDialog( this );
    instancesWidget_  = new InstancesWidget( parent );
    instancesWidget_->setCellViewer(this);

    cellsLib_ = new CellsLib(parent);
    cellsLib_ -> setCellViewer(this);

    setCentralWidget( cellWidget_ );

    /**
     * @brief Option of the bar menu
     * Options of the FILE menu
     * 
     */
    QMenu* fileMenu = menuBar()->addMenu("&File");

    QAction* action = new QAction ("&Open Cell", this);
    action->setStatusTip ("Open from disk a Cell");
    action->setShortcut (QKeySequence("CTRL+O"));
    action->setVisible(true);
    fileMenu->addAction(action);
    connect(action, SIGNAL(triggered()), this, SLOT(openCell()));

    action = new QAction ("&Show Instances", this);
    action->setStatusTip ("Show all Instances");
    action->setShortcut (QKeySequence("CTRL+I"));
    action->setVisible(true);
    fileMenu->addAction(action);
    connect(action, SIGNAL(triggered()), this, SLOT(showInstancesWidget()));

    action = new QAction ("&Show Cells", this);
    action->setStatusTip ("Show all Cells");
    action->setShortcut (QKeySequence("CTRL+C"));
    action->setVisible(true);
    fileMenu->addAction(action);
    connect(action, SIGNAL(triggered()), this, SLOT(showCellsLib()));

    action = new QAction ("&Save As", this);
    action->setStatusTip ("Save to disk (rename) the Cell");
    action->setShortcut (QKeySequence("CTRL+S"));
    action->setVisible(true);
    fileMenu->addAction(action);
    connect(action, SIGNAL(triggered()), this, SLOT(saveCell()));

    action = new QAction("&Quit", this);
    action->setStatusTip("Exit the Netlist Viewer");
    action->setShortcut(QKeySequence("CTRL+Q"));
    action->setVisible(true);
    fileMenu->addAction(action);
    connect(action, SIGNAL(triggered()), this, SLOT(close()));

    connect(this, SIGNAL(cellLoaded()), cellsLib_->getBaseModel(), SLOT(updateDatas()));
  }

  CellViewer::~CellViewer() {}

  /**
   * @brief Get cell of cell widget.
   * 
   * @return Cell* 
   */
  Cell* CellViewer::getCell() const {
    return cellWidget_->getCell();
  }

  /**
   * @brief Set cell to all elements in the programm
   * It show the new cell,
   * Calls set cell in instance widget to update list of instances
   * call set cell in cell widget to update list of cells
   * 
   * @param cell 
   */
  void CellViewer::setCell(Cell* cell) {
    cellWidget_->setCell(cell);
    instancesWidget_->setCell(cell);
  }

  /**
   * @brief call to save cell dialog window to save the current
   * cell with a new name
   * 
   */
  void CellViewer::saveCell() {
    Cell* cell = getCell();
    if(cell == NULL) return;

    QString cellName = cell->getName().c_str();

    if(saveCellDialog_->run(cellName)){
      cell->setName(cellName.toStdString());
      cell->save   (cellName.toStdString());
    }
  }

  /**
   * @brief Call to open cell dialog window to open a new cell
   * 
   */
  void CellViewer::openCell() {
    std::string s = "";
    QString cellName = s.c_str();

    if(openCellDialog_->run(cellName)){ 
      //Check if cell exist already
      for(auto* c : cellWidget_->getCell()->getAllCells()){
        if(c->getName() == cellName.toStdString()){
            std::cerr << "[INFO] Cell already in memory. Setting Cell: <" << cellName.toStdString() << ">." << std::endl;
            setCell(c);
            emit cellLoaded();
            return;
        }
      }

      //Cell not found
      Cell* cell = Cell::load(cellName.toStdString());
      if(cell){
        setCell(cell);
        emit cellLoaded();
      }
    }
  }

  /**
   * @brief call instances window show method
   * to show list fo instances in the current window
   * 
   */
  void CellViewer::showInstancesWidget() {
    instancesWidget_->show();
  }

  /**
   * @brief call showCellsWindow show method 
   * to show list fo cells loaded i memory
   * 
   */
  void CellViewer::showCellsLib() {
    cellsLib_->show();
  }

}