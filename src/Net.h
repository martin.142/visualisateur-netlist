/**
 * @file Net.h
 * @author ACOSTA CORRAL Martin Eduardo (martin.acostaec@gmail.com)
 * @author BOUHMID Zakaria              (zakaria.bmd@gmail.com)
 * @brief Data for connection between nodes / terms
 * @version 0.1
 * @date 2022-01-02
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef  NETLIST_NET_H   
#define  NETLIST_NET_H   
#include  <libxml/xmlreader.h>
#include <string>
#include <vector>
#include "Term.h"

namespace Netlist{
	
	class Cell;
	class Node;

	class Net{
	public:
																			Net     			( Cell*, std::string, Term::Type );
																		 ~Net     			();	
																		 
		inline const 	std::vector<Node*>& getNodes      () const;
		inline const 	std::string&        getName       () const;
		inline 		 		Cell*               getCell       () const;
		inline 		 		unsigned int        getId         () const;
		inline 		 		Term::Type          getType       () const;
		inline const	std::vector<Line*>& getLines			() const;
									size_t              getFreeNodeId () const; 

									void  							add    				( Node* );
									bool  							remove 				( Node* );
									void 								add						( Line* );
									bool								remove				( Line* );

		static				Net*					 			fromXml				(Cell*, xmlTextReaderPtr&);

	private:
		Cell*               owner_;		// Owner Cell
		std::string         name_;		// Name ID
		unsigned int        id_;			// id
		Term::Type          type_;		// Internla, externla
		std::vector<Node*>  nodes_;		// Nodes associated to this net

		//TME7
		std::vector<Line*> lines_;		//Lines connected to this net
	};
	
	inline const	std::vector<Line*>& Net::getLines	() const{ return lines_;}
	inline const 	std::string& 		 		Net::getName	() const{ return name_; }
	inline const 	std::vector<Node*>& Net::getNodes	() const{ return nodes_; }
	inline 		 		Cell* 				 			Net::getCell	() const{ return owner_;}
	inline 		 		unsigned int 		 		Net::getId 		() const{ return id_; }
	inline 		 		Term::Type 	 	 			Net::getType	() const{ return type_; }
	
} // Netlist Namespace

#endif