/**
 * @file Cell.cpp
 * @author ACOSTA CORRAL Martin Eduardo (martin.acostaec@gmail.com)
 * @author BOUHMID Zakaria              (zakaria.bmd@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2021-12-29
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include  <libxml/xmlreader.h>
#include  <libxml/xmlstring.h>
#include  <cstdlib>
#include  <vector>
#include  <fstream>
#include  "XmlUtil.h"
#include  "Cell.h"
#include  "Term.h"
#include  "Net.h"
#include  "Line.h"


namespace Netlist {

  using namespace std;

  //Static vector of cells
  vector<Cell*>  Cell::cells_;

  Cell::Cell ( const string& name )
    : symbol_(this) //TME7
    , name_     (name) 
    , terms_    ()
    , instances_()
    , nets_     ()
    , maxNetIds_(0)
  {
    if (find(name)) {
      cerr << "[ERROR] Attempt to create duplicate of Cell <" << name << ">.\n"
           << "        Aborting..." << endl;
      exit( 1 );
    } 
      
    cells_.push_back( this );
  }


  Cell::~Cell ()
  {
    //Remove the current Cell object form static variable
    for ( vector<Cell*>::iterator icell=cells_.begin() ; icell != cells_.end() ; ++icell ) {
      if (*icell == this) {
        cells_.erase( icell );
        break;
      }
    }

    //Rmove objects from vectors
    while ( not nets_     .empty() ) delete *nets_     .begin();
    while ( not instances_.empty() ) delete *instances_.begin();
    while ( not terms_    .empty() ) delete *terms_    .begin();
  }

  /**
   * @brief search a cell by name 
   * 
   * @param name  Name of cell to seach
   * @return Cell*  return cell found or NULL
   */
  Cell* Cell::find ( const string& name )
  {
    for (auto * c : cells_ ) {
      if (c->getName() == name) return c;
    }
    return NULL;
  }

  /**
   * @brief Search an instance in the vector of instances. If not found NULL
   * 
   * @param name        Instance searched
   * @return Instance*  Instance found or NULL
   */
  Instance* Cell::getInstance ( const std::string& name ) const
  {
    for ( auto * i : instances_ ) {
      if (i->getName() == name)  return i;
    }
    return NULL;
  }

  /**
   * @brief Search an cell in the vector of cells. If not found NULL
   * 
   * @param name    Term seached
   * @return Term*  Return pointer of term found or NULL
   */
  Term* Cell::getTerm ( const std::string& name ) const
  {
    for ( auto * t : terms_ ) {
      if (t->getName() == name)  return t;
    }
    return NULL;
  }

  /**
   * @brief Search an net in the vector of net. If not found NULL
   * 
   * @param name  Term Seached
   * @return Net* return pointer to net found or NULL
   */
  Net* Cell::getNet ( const std::string& name ) const
  {
    for ( auto * n : nets_ ) {
      if (n->getName() == name)  return n;
    }
    return NULL;
  }

  /**
   * @brief Change new name current cell
   * 
   * @param cellName New name of cell
   */
  void  Cell::setName ( const string& cellName )
  {
    if (cellName == name_) return;
    if (find(cellName) != NULL) {
      cerr << "[ERROR] Cell::setName() - New Cell name <" << cellName << "> already exists."<< endl;
      return;
    }
    name_ = cellName;
  }

  /**
   * @brief OVERLOADED FUNCTION
   * Adds an instance to the vector of instances if not already present
   * 
   * @param instance Instance to ADD
   */
  void  Cell::add ( Instance* instance )
  {
    if (getInstance(instance->getName())) {
      cerr << "[ERROR] Attemp to add duplicated instance <" << instance->getName() << ">." << endl;
      exit( 1 );
    }
    instances_.push_back( instance );
  }


  /**
   * @brief FUNCTION OVERLOADED
   * Adds a term to the vector of therms if not present
   * 
   * @param term Term to add
   */
  void  Cell::add ( Term* term )
  {
    if (getTerm(term->getName())) {
      cerr << "[ERROR] Attemp to add duplicated terminal <" << term->getName() << ">." << endl;
      exit( 1 );
    }
    terms_.push_back( term );
  }


  /**
   * @brief FUNCTION OVERLOADED
   * Adds a term to the vector of terms of the current cell if not already present
   * 
   * @param net Net to add
   */
  void  Cell::add ( Net* net )
  {
    if (getNet(net->getName())) {
      cerr << "[ERROR] Attemp to add duplicated Net <" << net->getName() << ">." << endl;
      exit( 1 );
    }
    nets_.push_back( net );
  }

  /*
   *
   * 
   */


  /**
   * @brief Create a link between term with a given name (if exists)
   * and a net
   * 
   * @param name Name of term link
   * @param net  Net to linkg with ther (if found)
   * @return true   Term Not found 
   * @return false  Term found and linked with net
   */
  bool  Cell::connect ( const string& name, Net* net )
  {
    Term* term = getTerm( name );
    if (term == NULL) 
      return false;
    term->setNet( net );
    return true;
  }


  /**
   * @brief FUNCTION OVERLOADED
   * Removes the given argument (INSTANCE, TERM OR NET) from 
   * its vector
   * 
   * @param instance 
   */
  void  Cell::remove ( Instance* instance )
  {
    for(auto i = instances_.begin(); i != instances_.end(); i++ ){
      if (*i == instance) instances_.erase( i );
    }
  }
  void  Cell::remove ( Term* term )
  {
    for ( auto t = terms_.begin(); t != terms_.end(); t++) {
      if (*t == term) terms_.erase( t );
    }
  }
  void  Cell::remove ( Net* net )
  {
    for ( auto n = nets_.begin(); n != nets_.end(); n++ ) {
      if (*n == net) nets_.erase( n );
    }
  }

  unsigned int Cell::newNetId ()
  { return maxNetIds_++; }

  /**
   * @brief STATIC FUNCTION
   * Return all cll that have been created / opened during the execution
   * of the programm
   * attribute cells_ is also static
   * 
   * @return std::vector<Cell*>& 
   */
  std::vector<Cell*>& Cell::getAllCells(){return cells_;}






 /*
   * ================================================================================================================
   * ================================================================================================================
   * ======================================                          ================================================
   * ====================================== READ AND WRITE XML FILES ================================================
   * ======================================                          ================================================
   * ================================================================================================================
   * ================================================================================================================
   */







  /**
   * @brief Print data structure to the console. 
   * toXml() print function.
   * Class Term, Instance & Node have their own
   * 
   * @param stream 
   */
  void Cell::toXml(std::ostream& stream) const {
  
      stream << "<?xml version = \"1.0\"?>\n";
      stream << indent++ << "<cell name = \"" << this->name_ << "\">\n";

      //TERMS
      stream << indent++ << "<terms>\n";
      for(Term* t : terms_){
        t->toXml(stream);
      }
      stream << --indent << "</terms>\n";

      //INSTANCES
      stream << indent++ << "<instances>\n";
      for(Instance* in : instances_){
        in->toXml(stream);
      }
      stream << --indent << "</instances>\n";

      //  NETS
      stream << indent++ << "<nets>\n";
      for(Net* net : nets_){
          
          stream << indent++  <<  "<net name=\""  << net->getName()                 << "\"" 
                              << " type=\""       << Term::toString(net->getType()) << "\">\n";
          for(Node* node : net->getNodes()){
              if(node != NULL)
                node->toXml(stream);
          }
          for(Line* line : net->getLines()){
              //cout << "LINE FOUND" << endl;
              if(line != NULL)
                line->toXml(stream);
          }
          stream << --indent << "</net>\n";
      }
      stream << --indent << "</nets>\n";

      symbol_.toXml(stream);  //TME7

      stream << --indent << "</cell>\n\n";
  }


  
  /**
   * @brief read XML file to add a new cell
   * This is the main function, this function idetify the labels and
   * call the fromXml() function that corresponds to each type.
   * 
   * @param reader 
   * @return Cell* 
   */
  Cell* Cell::fromXml ( xmlTextReaderPtr reader )
  {
  enum  State { Init           = 0
              , BeginCell
              , BeginTerms
              , EndTerms
              , BeginInstances
              , EndInstances
              , BeginNets
              , EndNets
              , BeginSymbol 
              , EndSymbol 
              , EndCell
              };

  //Tags to seach in XML file
  const xmlChar* cellTag      = xmlTextReaderConstString( reader, (const xmlChar*)"cell" );
  const xmlChar* netsTag      = xmlTextReaderConstString( reader, (const xmlChar*)"nets" );
  const xmlChar* termsTag     = xmlTextReaderConstString( reader, (const xmlChar*)"terms" );
  const xmlChar* instancesTag = xmlTextReaderConstString( reader, (const xmlChar*)"instances" );
  const xmlChar* symbolTag    = xmlTextReaderConstString( reader, (const xmlChar*)"symbol" );
  const xmlChar* netTag       = xmlTextReaderConstString( reader, (const xmlChar*)"net" );
  const xmlChar* nodeTag      = xmlTextReaderConstString( reader, (const xmlChar*)"node" );
  const xmlChar* lineTag      = xmlTextReaderConstString( reader, (const xmlChar*)"line" );

  Cell* cell   = NULL;
  Net *netTmp  = NULL;
  State state  = Init;

  // ininite bucle, 
  // While breakes when we arrive to end TAG for xml file or error
  while ( true ) {
    
    int status = xmlTextReaderRead(reader);
    if (status != 1) {
      if (status != 0) {
        cerr << "[ERROR] Cell::fromXml(): Unexpected termination of the XML parser." << endl;
      }
      break;
    }

    //Skip comments, whitespace and tabs
    switch ( xmlTextReaderNodeType(reader) ) {
      case XML_READER_TYPE_COMMENT:
      case XML_READER_TYPE_WHITESPACE:
      case XML_READER_TYPE_SIGNIFICANT_WHITESPACE:
        continue;
    }

    //Each iteration, nodeName take a new tag
    const xmlChar* nodeName = xmlTextReaderConstLocalName( reader );

    //State machine structure.
    switch ( state ) {
      case Init:
        if (cellTag == nodeName) {
          state = BeginCell;
          string cellName = xmlCharToString(xmlTextReaderGetAttribute( reader, (const xmlChar*)"name" ) );
          if (not cellName.empty()) {
            cell = new Cell ( cellName );
            state = BeginTerms;
            continue;
          }
        }
        break;
      case BeginTerms:// ----------------- Begin of terms
        if ( (nodeName == termsTag) and (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT) ) {
          state = EndTerms;
          continue;
        }
        break;
      case EndTerms:// ----------------- Wait for Term end tag
        if ( (nodeName == termsTag) and (xmlTextReaderNodeType(reader) == XML_READER_TYPE_END_ELEMENT) ) {
          state = BeginInstances;
          continue;
        } else {
          if (Term::fromXml(cell,reader)) continue; //Term found
        }
        break;
      case BeginInstances:// ------------ Begin of instance
        if ( (nodeName == instancesTag) and (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT) ) {
          state = EndInstances;
          continue;
        }
        break;
      case EndInstances:// --------------- Wait for instanc end Tag
        if ( (nodeName == instancesTag) and (xmlTextReaderNodeType(reader) == XML_READER_TYPE_END_ELEMENT) ) {
          state = BeginNets;
          continue;
        } else {
          if (Instance::fromXml(cell,reader)) continue; // Instance Found
        }
        break;
      case BeginNets:// -------------------- Begin of nets
        if ( (nodeName == netsTag) and (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT) ) {
          state = EndNets;
          continue;
        }
        break;
      case EndNets:// ----------------------- wait for Net end tags
        if ( (nodeName == netsTag) and (xmlTextReaderNodeType(reader) == XML_READER_TYPE_END_ELEMENT) ) {
          state = BeginSymbol;
          continue;
        } else if(nodeName == netTag and (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT)){
          if ((netTmp = Net::fromXml(cell,reader))) { // Net found
            continue;
          }
        } else if (nodeName == nodeTag and (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT)){
          if (Node::fromXml(cell,reader, netTmp)) {   //Node trouvé
            continue;
          }
        } else if (nodeName == lineTag and (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT)){
          if (Line::fromXml(netTmp,reader)) {         //Line Found
            continue;
          }
        }else if(nodeName == netTag and (xmlTextReaderNodeType(reader) == XML_READER_TYPE_END_ELEMENT)){
          continue;
        }
        break;
      case BeginSymbol: // ------------------- Beigin of Symbosl
        if((nodeName == symbolTag) and (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT)){
          state = EndSymbol;
          continue;
        }
        break;
      case EndSymbol: // -------------------- Wait for Symbol end tag
        if((nodeName == symbolTag) and (xmlTextReaderNodeType(reader) == XML_READER_TYPE_END_ELEMENT)){        
            state = EndCell;
            continue;
        } else {
          if(Symbol::fromXml(cell, reader)){    ///Symbol found
            continue;
          }
        }
        break;
      case EndCell:   // ------------------- Wait for Cell end tag
        if ( (nodeName == cellTag) and (xmlTextReaderNodeType(reader) == XML_READER_TYPE_END_ELEMENT) ) {
          continue;
        }
        break;
      default:
        break;
    }

    cerr << "[ERROR] Cell::fromXml(): Unknown or misplaced tag <" << nodeName
         << "> (line:" << xmlTextReaderGetParserLineNumber(reader) << ")." << endl;
    break;
  }

  return cell;
}

/**
 * @brief Load XML File, then calls fromXml
 * 
 * @param cellName XML File to open
 * @return Cell*   cell Created at the en of fromXml
 */
Cell* Cell::load ( const std::string cellName )
{
  string cellFile = "../work/cells/" + cellName + ".xml";
  xmlTextReaderPtr reader;

  reader = xmlNewTextReaderFilename( cellFile.c_str() );
  if (reader == NULL) {
    cerr << "[ERROR] Cell::load() unable to open file <" << cellFile << ">." << endl;
    return NULL;
  }

  Cell* cell = Cell::fromXml( reader );
  xmlFreeTextReader( reader );

  return cell;
}


/**
 * @brief Create the XML File of the curent 
 * 
 */
void  Cell::save () const
{
  string  fileName   = getName() + "_created.xml";
  fstream fileStream ( fileName.c_str(), ios_base::out|ios_base::trunc );
  if (not fileStream.good()) {
    cerr << "[ERROR] Cell::save() unable to open file <" << fileName << ">." << endl;
    return;
  }

  cerr << "Saving <Cell " << getName() << "> in <" << fileName << ">" << endl;
  toXml( fileStream );

  fileStream.close();
}


/**
 * @brief Create the XML file of the current cell with a custom name
 * 
 * @param cellName  custom name for file
 */
void  Cell::save (std::string cellName) const
{
  string  fileName   = cellName + "_created.xml";
  fstream fileStream ( fileName.c_str(), ios_base::out|ios_base::trunc );
  if (not fileStream.good()) {
    cerr << "[ERROR] Cell::save() unable to open file <" << fileName << ">." << endl;
    return;
  }

  cerr << "Saving <Cell " << getName() << "> in <" << fileName << ">" << endl;
  toXml( fileStream );

  fileStream.close();
}

}  // Netlist namespace.
