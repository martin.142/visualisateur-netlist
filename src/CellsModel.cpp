/**
 * @file CellsModel.cpp
 * @author ACOSTA CORRAL Martin Eduardo (martin.acostaec@gmail.com)
 * @author BOUHMID Zakaria              (zakaria.bmd@gmail.com)
 * @brief Function to show data in the list of cells
 * @version 0.1
 * @date 2022-01-02
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <QString>
#include "InstancesModel.h"
#include  "Net.h"
#include  "Instance.h"
#include <iostream>

#include "Cell.h"
#include "CellsModel.h"


namespace Netlist {

  using namespace std;

  CellsModel::CellsModel( QObject* parent )
    : QAbstractTableModel(parent)
    , cells_( )
  {
    
  }

  CellsModel::~CellsModel() {}

  /**
   * @brief Stop and start data refreshing to update new cell data
   * 
   */
  void  CellsModel::updateDatas()
  {
    emit layoutAboutToBeChanged();
    emit layoutChanged();
  }

  /**
   * @brief get the number of row depending in the size of the static vector of cells
   * 
   * @param parent 
   * @return int 
   */
  int CellsModel::rowCount(const QModelIndex &parent) const 
  {
    return Cell::getAllCells().size();
  }

  /**
   * @brief Only 1 column to show the cell in memory
   * 
   * @param parent 
   * @return int 
   */
  int CellsModel::columnCount(const QModelIndex &parent) const
  {
    return 1;
  }

  /**
   * @brief Read data from vector of cells and return their names
   * 
   * @param index 
   * @param role 
   * @return QVariant 
   */
  QVariant CellsModel::data(const QModelIndex &index, int role) const
  {
    if (Cell::getAllCells().size()==0 or not index.isValid ()) {
      return QVariant ();
    }
    if (role == Qt:: DisplayRole) {
      int row = index.row();
      switch( index.column() ) {
        case 0: return Cell::getAllCells()[row]->getName().c_str();
      }
    }
    return QVariant ();
  }

  /**
   * @brief Set te text in the header of the list
   * ONLY calse 0 because only one column
   * 
   * @param section 
   * @param orientation 
   * @param role 
   * @return QVariant 
   */
  QVariant CellsModel::headerData(int section, Qt::Orientation orientation, int role) const
  {
    if (orientation == Qt:: Vertical) return QVariant ();
    if (role != Qt:: DisplayRole) return QVariant ();
    switch ( section ) {
      case 0: return "Cells";
    }
    return QVariant ();
  }

  /**
   * @brief get cell of the row selected
   * 
   * @param row 
   * @return Cell*  cell returned, NULL if nothing selected
   */
  Cell* CellsModel::getModel ( int row )
  {
    if (Cell::getAllCells().size()==0) return NULL;
    if ( row >= (int)Cell::getAllCells().size() ) return NULL;
    return Cell::getAllCells()[row];
  }

}  // Netlist namespace.