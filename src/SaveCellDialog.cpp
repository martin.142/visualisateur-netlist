/**
 * @file SaveCellDialog.cpp
 * @author ACOSTA CORRAL Martin Eduardo (martin.acostaec@gmail.com)
 * @author BOUHMID Zakaria              (zakaria.bmd@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2022-01-02
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QDialog>
#include "SaveCellDialog.h"

namespace Netlist
{
  SaveCellDialog::SaveCellDialog ( QWidget * parent )
  : QDialog ( parent )
  , lineEdit_ ( NULL )
  {
    /**
     * @brief objects to be place in the window
     * Window Title
     * Label
     * Line Edit
     * OK Push button
     * Cancel Push Button
     */
    setWindowTitle ( tr ( " Save Cell ") );
    QLabel * label = new QLabel ();
    label -> setText ( tr (" Enter Cell name ( without extention )") );
    lineEdit_ = new QLineEdit ();
    lineEdit_ -> setMinimumWidth ( 400 );
    QPushButton * okButton = new QPushButton ();
    okButton -> setText ( " OK " );
    okButton -> setDefault ( true );
    QPushButton * cancelButton = new QPushButton ();
    cancelButton -> setText ( " Cancel " );

    /**
     * @brief Layout of the window
     * Horisontal layout.  Strech = |
     *    | OK buton | Cancel Button |  
     * Vertical Layout 
     *    Label
     *    Line Edit 
     *    Horisontal Layout
     * 
     * link signals to buttons
     * 
     */
    QHBoxLayout * hLayout = new QHBoxLayout ();
    hLayout -> addStretch ();
    hLayout -> addWidget ( okButton );
    hLayout -> addStretch ();
    hLayout -> addWidget ( cancelButton );
    hLayout -> addStretch ();
    QFrame * separator = new QFrame ();
    separator -> setFrameShape ( QFrame :: HLine );
    separator -> setFrameShadow ( QFrame :: Sunken );
    QVBoxLayout * vLayout = new QVBoxLayout ();
    vLayout -> setSizeConstraint ( QLayout :: SetFixedSize );
    vLayout -> addWidget ( label );
    vLayout -> addWidget ( lineEdit_ );
    vLayout -> addLayout ( hLayout );
    setLayout ( vLayout );

    connect ( okButton , SIGNAL ( clicked ()) , this , SLOT ( accept ()) );
    connect ( cancelButton , SIGNAL ( clicked ()) , this , SLOT ( reject ()) );
  }

  /**
   * @brief Read text written in the line text
   * 
   * @return const QString Text written in the line text
   */
  const QString SaveCellDialog::getCellName () const
  { return lineEdit_->text (); }

  /**
   * @brief Write into the line text
   *  
   * @param name text to write in the line text
   */
  void SaveCellDialog::setCellName ( const QString & name )
  { return lineEdit_->setText ( name ); }

  /**
   * @brief Waits for pushing the ok or cancel button.
   * 
   * @param name    Current name of cell
   * @return true   OK button clicked
   * @return false  Cancle button clicked
   */
  bool SaveCellDialog::run ( QString & name )
  {
    setCellName ( name );
    int dialogResult = exec ();
    name = getCellName ();
    return ( dialogResult == Accepted );
  }
} // namespace Netlist
