/**
 * @file Instance.cpp
 * @author ACOSTA CORRAL Martin Eduardo (martin.acostaec@gmail.com)
 * @author BOUHMID Zakaria              (zakaria.bmd@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2022-01-02
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include  <libxml/xmlreader.h>
#include <ostream>
#include  <vector>
#include  "Net.h"
#include  "Instance.h"
#include  "Cell.h"
#include "XmlUtil.h"
#include "Shape.h"

namespace Netlist{

  Instance::Instance (Cell* owner, Cell* masterCell, std::string name)
  : owner_(owner), 
    masterCell_(masterCell), 
    name_(name),
    terms_(),
    position_()
  {
    owner_->add(this);

    /**
     * @brief Copy all terms from masterCell by calling the constructor
     * of Term(Instance). Same data, Type Internal
     * 
     */
    for(Term* t : masterCell->getTerms()){
        add(new Term(this,t)); 
    } 
  }

  Instance::~Instance(){
    for(Term* t : terms_){
      delete t;
    }  
  }

  /**
   * @brief Get the ponter to a term with the given name
   * 
   * @param name  Term name to be seache in the instance
   * @return Term*  Pointer to a term, NULL if not found
   */
  Term* Instance::getTerm(std::string name) const{
    for(Term* t : terms_){
        if(name == t->getName())
            return t;
    } 
    return nullptr;
  }


  /**
  * @brief Connect internal term if it exists to a Net
  * 
  * @param name   Connect the term with the given name to the given net
  * @param net    Pointer to the net to be connected
  * @return true  The net was succesfuly assigned to the current term
  * @return false Term not found
  */
  bool Instance::connect(std::string name, Net* net){
    Term* term = getTerm(name);
    if (term == NULL)
        return false;      
    term->setNet( net );
    return true;
  }

  /**
   * @brief Add a term to the vector of terms if ther not already present
   * 
   * @param term Term to be added. 
   */
  void Instance::add (Term* term){
    if (getTerm(term->getName())) {
      return;
    }
    terms_.push_back( term );
  }

  /**
   * @brief Remove term from vector of terms
   * 
   * @param term term to be removed
   */
  void Instance::remove(Term* term){
    for ( std::vector<Term*>::iterator iterm=terms_.begin() ; iterm != terms_.end() ; ++iterm ) {
        if (*iterm == term) terms_.erase( iterm );
    }
  }

  /**
   * @brief Set position to the instance poisitionn
   * 
   * @param p point to be set
   */
  void Instance::setPosition(const Point& p){
    position_.setX(p.getX());
    position_.setY(p.getY());
  }

  /**
   * @brief FUNCTION OVERLOADED
   * Create a point with the given x, y coordianates and set te position
   * 
   * @param x 
   * @param y 
   */
  void Instance::setPosition(int x, int y){
    setPosition(Point(x,y));
  }





  /*
   * ================================================================================================================
   * ================================================================================================================
   * ======================================                          ================================================
   * ====================================== READ AND WRITE XML FILES ================================================
   * ======================================                          ================================================
   * ================================================================================================================
   * ================================================================================================================
   */





  void Instance::toXml(std::ostream& stream){
    stream << indent  << "<instance name=\""  << getName()                  << "\"" 
                      << " mastercell=\""     << getMasterCell()->getName() << "\"" 
                      << " x=\""              << getPosition().getX()       << "\"" 
                      << " y=\""              << getPosition().getY()       << "\"/>\n";
  }

  Instance* Instance::fromXml(Cell* cell, xmlTextReaderPtr reader){
    std::string InstanceName  =            xmlCharToString(xmlTextReaderGetAttribute( reader, (const xmlChar*)"name" ) );
    Cell*       masterCell    = Cell::find(xmlCharToString(xmlTextReaderGetAttribute( reader, (const xmlChar*)"mastercell" ) ));
    int         x             = std::stoi( xmlCharToString(xmlTextReaderGetAttribute( reader, (const xmlChar*)"x")));
    int         y             = std::stoi( xmlCharToString(xmlTextReaderGetAttribute( reader, (const xmlChar*)"y"))); 
    
    if(InstanceName.empty() or masterCell == NULL  )
      return NULL;

    Instance* instance = new Instance(cell, masterCell, InstanceName);
    instance->setPosition(x,y);
    return instance;
  }

} // namespace netlist