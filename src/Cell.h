/**
 * @file Cell.h
 * @author ACOSTA CORRAL Martin Eduardo (martin.acostaec@gmail.com)
 * @author BOUHMID Zakaria              (zakaria.bmd@gmail.com)
 * @brief Cell class declarations
 * Main object in the project, it has;
 *  static vector of cells, visible from within the class
 *  Name ======> Original name and to be identified
 *  Terms =====> Connections. Externals and internals for instances
 *  Instances => Use of other component in this cell
 *  Nets ======> Cables / lines of connectios
 *  
 * @version 0.1
 * @date 2022-01-02
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef NETLIST_CELL_H
#define NETLIST_CELL_H
#include  <libxml/xmlreader.h>
#include <string>
#include <vector>
#include <iostream>
#include "Indentation.h"
#include "Symbol.h"   //TME 7

namespace Netlist {

  class Instance;
  class Net;
  class Term;

  class Cell {
    public:
      static       std::vector<Cell*>&      getAllCells       ();
      static       Cell*                    find              ( const std::string& );
    public:                                                  
                                            Cell              ( const std::string& );
                                            ~Cell             ();

      inline       std::string              getName           () const;
      inline       Symbol*                  getSymbol         () const;   //TME 7
      inline const std::vector<Instance*>&  getInstances      () const;
      inline const std::vector<Term*>&      getTerms          () const;
      inline const std::vector<Net*>&       getNets           () const;
                  Instance*                 getInstance       ( const std::string& ) const;
                  Term*                     getTerm           ( const std::string& ) const;
                  Net*                      getNet            ( const std::string& ) const;

                  void                      setName           ( const std::string& );
                  void                      add               ( Instance* );
                  void                      add               ( Term* );
                  void                      add               ( Net* );
                  void                      remove            ( Instance* );
                  void                      remove            ( Term* );
                  void                      remove            ( Net* );
                   
                  bool                      connect           ( const std::string& name, Net* net );
                  unsigned int              newNetId          ();
                  void                      toXml             (std::ostream& stream) const;
     static       Cell*                     fromXml           (xmlTextReaderPtr reader);
     static       Cell*                     load              (std::string cellName);
                  void                      save              () const;
                  void                      save              (std::string) const;
    private:
      static  std::vector<Cell*>      cells_;
              Symbol                  symbol_;
              std::string             name_;
              std::vector<Term*>      terms_;
              std::vector<Instance*>  instances_;
              std::vector<Net*>       nets_;
              unsigned int            maxNetIds_;
  };

  inline       Symbol*                 Cell::getSymbol    () const {return const_cast<Symbol*>(&symbol_ );}  //TME7
  inline       std::string             Cell::getName      () const { return name_; }
  inline const std::vector<Instance*>& Cell::getInstances () const { return instances_; };
  inline const std::vector<Term*>&     Cell::getTerms     () const { return terms_; };
  inline const std::vector<Net*>&      Cell::getNets      () const { return nets_; };


}  // Netlist namespace.

#endif  // NETLIST_CELL_H