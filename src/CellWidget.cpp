/**
 * @file CellWidget.cpp
 * @author ACOSTA CORRAL Martin Eduardo (martin.acostaec@gmail.com)
 * @author BOUHMID Zakaria              (zakaria.bmd@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2021-12-29
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include  <QResizeEvent>
#include  <QPainter>
#include  <QPen>
#include  <QBrush>
#include  <QFont>
#include  <QApplication>
#include <QKeyEvent>
#include <QPoint>
#include  "CellWidget.h"
#include  "Term.h"
#include  "Instance.h"
#include  "Symbol.h"
#include  "Shape.h"
#include  "Cell.h"
#include  "Line.h"
#include  "Node.h"
#include  "Net.h"


#define LEFT  0x01000012
#define UP    0x01000013
#define RIGHT 0x01000014
#define DOWN  0x01000015
#define TERM_TEXT_SZ 10



namespace Netlist {

  using namespace std;

  /**
   * @brief Overload of operator << to print parameters of a rect
   * 
   */
  ostream& operator<< ( ostream& o, const QRect& rect )
  {
    o << "<QRect x:" << rect.x()
      <<       " y:" << rect.y()
      <<       " w:" << rect.width()
      <<       " h:" << rect.height() << ">";
    return o;
  }

  /**
   * @brief Overload of operator << to print parameters of a point
   * 
   */
  ostream& operator<< ( ostream& o, const QPoint& p )
  { o << "<QRect x:" << p.x() << " y:" << p.y() << ">"; return o; }


  CellWidget::CellWidget ( QWidget* parent )
    : QWidget(parent)
    , cell_  (NULL)
  {
    setAttribute    ( Qt::WA_OpaquePaintEvent );
    setAttribute    ( Qt::WA_NoSystemBackground );
    setAttribute    ( Qt::WA_StaticContents );
    setSizePolicy   ( QSizePolicy::Expanding, QSizePolicy::Expanding );
    setFocusPolicy  ( Qt::StrongFocus );
    setMouseTracking( true );
  }


  CellWidget::~CellWidget ()
  { }

  /**
   * @brief Set a Cell as curent to show data
   * 
   * @param cell Cell to be set a current Cell in the widget
   */
  void  CellWidget::setCell(Cell* cell )
  {
    std::cout << "CellWidget - cell->getName(): " << cell->getName() << std::endl;
    cell_ = cell;
    cellPosition();
    repaint();
  }
  /**
   * @brief Sets minimun size of widget in the window
   * 
   * @return QSize XX
   */
  QSize  CellWidget::minimumSizeHint () const
  { return QSize(600,600); }

  /**
   * @brief QT Event
   * Function called by QT when main window resized 
   * using cursor
   * 
   * @param event 
   */
  void  CellWidget::resizeEvent ( QResizeEvent* event )
  {  
    repaint();
    cellPosition();
  }

  void CellWidget::cellPosition(){

    //size of the symbol and the mid part
    int w  = cell_->getSymbol()->getBoundingBox().getWidth();
    int h  = cell_->getSymbol()->getBoundingBox().getHeight();
    if(w > 0) h /= 2;
    if(h > 0) w /= 2;


    //size of the screen and midpart
    int x1 = screenRectToBox(rect()).getX1();
    int y1 = screenRectToBox(rect()).getY1();
    int x2 = screenRectToBox(rect()).getX2();
    int y2 = screenRectToBox(rect()).getY2();

    int dx = x2 - x1;
    int dy = y2 - y1;

    int posCellX = (x1 - (dx / 2) + w);
    int posCellY = (y1 - ( dy / 2 ) + h);

    viewport_.setX1(posCellX);
    viewport_.setY1(posCellY);
    viewport_.setX2(posCellX + dx);
    viewport_.setY2(posCellY + dy);
  }

  /**
   * @brief QT Event
   * Function called when call repaint() or by QT
   * 
   * @param event 
   */
  void  CellWidget::paintEvent ( QPaintEvent* event )
  {
    
    QFont  bigFont = QFont( "URW Bookman L", 36 );

    QString cellName = "NULL";
    if (cell_) cellName = cell_->getName().c_str();

    QPainter painter(this);
    painter.setFont      ( bigFont );
    painter.setBackground( QBrush( Qt::black) );
    painter.setPen(Qt::white);
    painter.eraseRect    ( QRect( QPoint(0,0), size() ) );
    
    int nbInstances = cell_->getInstances().size();
    if(nbInstances != 0){
      for(auto * i : cell_->getInstances()){
        drawCells(i, painter);
      }
      for(auto* t : cell_->getTerms()){
        drawTerm(t, painter, 10);
      }
      drawSide(cell_, painter);
      drawLine(cell_, painter);
      drawNodePoints(cell_, painter);
    } else{
      drawCells(cell_, painter, 0, 0, true);
    }
  }

  /**
   * @brief QT Event
   * Function called when keyboard pressed depending on wich arrow
   * key were pressed, the corresponding function is called.
   * 
   * @param event 
   */
  void CellWidget::keyPressEvent (QKeyEvent* event){
    if(event->key() == LEFT){
      goLeft();
    }
    if(event->key() == UP){
      goUp();
    }
    if(event->key() == RIGHT){
      goRight();
    }
    if(event->key() == DOWN){
      goDown();
    }
  }

  /**
   * @brief Fucntion called when event of mouse click ocurred in the cell widget area.
   * Depending on the position of click, the function to move viewpor will be called.
   * 
   * @param event 
   */
  void CellWidget::mousePressEvent ( QMouseEvent * event ){
    // ======> UP & LEFT
    if(event->x() < 70 and event->y() < 70){ 
      goUp();
      goLeft();
    } 
    // ======> DOWN & LEFT
    else if(event->x() < 70 and event->y() > screenRectToBox(rect()).getY2()-70){ 
      goDown();
      goLeft();
    } 
    // ======> DOWN & RIGHT
    else if(event->x() > screenRectToBox(rect()).getX2()-70 and event->y() > screenRectToBox(rect()).getY2()-70){
      goDown();
      goRight();
    }
    // ======> UP & RIGHT
    else if(event->x() > screenRectToBox(rect()).getX2()-70 and event->y() < 70){
      goUp();
      goRight();
    }
    // ======> LEFT
    else if(event->x() < 70){ 
      goLeft();
    } 
    // ======> UP
    else if(event->y() < 70){ 
      goUp();
    } 
    // ======> RIGHT
    else if(event->x() > screenRectToBox(rect()).getX2()-70){ 
      goRight();
    } 
    // ======> DOWN
    else if(event->y() > screenRectToBox(rect()).getY2()-70){ 
      goDown();
    } 

  }

  void CellWidget::goLeft(){
    viewport_.translate(20,0);
    repaint();
  }

  void CellWidget::goRight(){
    viewport_.translate(-20,0);
    repaint();
  }
  
  void CellWidget::goUp(){
    viewport_.translate(0,-20);
    repaint();
  }

  void CellWidget::goDown(){
    viewport_.translate(0,20);
    repaint();
  }

  


  

  //===================================================================================
  //===================================================================================
  //============= FUNCIONS USED TO DRAW THE DIAGRAM INTO THE WIDGET ===================
  //===================================================================================
  //===================================================================================

  /**
   * @brief Draw cell paint into cellWidget the diagram by reading the herited classes from shapes
   * BoxShape, LineShape, ArcShape, EllipseShape, TermShape
   * To identify the type of Shape, a dynamic_cast is performed for every shape to every type of shape.
   * the value of the type casted will be different to NULL if it correspond to that shape.
   * 
   * @param cell        Main Cell
   * @param painter     painter as REFERENCE
   * @param dxInstance  Position of instance x
   * @param dyInstance  Position of instance y
   * @param drawName    Indicate to print cellname.
   */
  void CellWidget::drawCells(const Cell* cell, QPainter& painter, int dxInstance, int dyInstance, bool drawName ) {
    int dxScreen, dyScreen;

    //True when it a cell without instances
    if(drawName){
      drawCellName(cell, painter, 100, 100);
    }

    //Iterate all Shapes of a single cell
    for(auto * s : cell->getSymbol()->getShapes()){

      /*
       *    BOX SHAPE
       */
      BoxShape * boxS = dynamic_cast<BoxShape*>(s);
      if(boxS != NULL){
        dxScreen = xToScreenX(boxS->getX1()) - boxS->getX1() + dxInstance;
        dyScreen = yToScreenY(boxS->getY1()) - boxS->getBoundinBox().getHeight() - dyInstance;
        painter.drawRect(boxToScreenRect(boxS->getBoundinBox().translate(dxScreen,dyScreen)));
      }
      
      /*
       *    LINE SHAPE
       */
      LineShape * lineS = dynamic_cast<LineShape*>(s);
      if(lineS != NULL){
        painter.setPen(Qt::green);
        painter.drawLine(xToScreenX(lineS->getX1() + dxInstance)
                       , yToScreenY(lineS->getY1() + dyInstance)
                       , xToScreenX(lineS->getX2() + dxInstance)
                       , yToScreenY(lineS->getY2() + dyInstance));
      }

      /*
       *    TERM SHAPE
       */
      TermShape * termS = dynamic_cast<TermShape*>(s);
      if(termS != NULL){
        //Draw TermShape
        painter.setFont(QFont("Arial", TERM_TEXT_SZ));
        painter.setPen(Qt::white);
        Box termBox(0,0,6,6);
        dxScreen = xToScreenX(termS->getX1()) - (termBox.getWidth()/2)  + dxInstance;
        dyScreen = yToScreenY(termS->getY1()) -(termBox.getHeight()/2)  - dyInstance;
        painter.fillRect(boxToScreenRect(termBox.translate(dxScreen,dyScreen)),Qt::red);  

        //Write Term shape name
        string nameTermStr = termS->getTerm()->getName();
        int strLen = strlen(nameTermStr.c_str());

        QString nameTermQs = nameTermStr.c_str();
        TermShape::NameAlign pos = termS->getNameAlign();
        if(pos == TermShape::top_left){
          painter.drawText(termBox.getX1() - (TERM_TEXT_SZ * strLen), termBox.getY1(), nameTermQs);
        }else if(pos == TermShape::top_right){
          painter.drawText(termBox.getX2(), termBox.getY1() - (TERM_TEXT_SZ/2), nameTermQs);
        }else if(pos == TermShape::bottom_left){
          painter.drawText(termBox.getX1() - (TERM_TEXT_SZ * strLen), termBox.getY2(), nameTermQs);
        }else if(pos == TermShape::bottom_right){
          painter.drawText(termBox.getX2(), termBox.getY2()+TERM_TEXT_SZ, nameTermQs);
        }
      }

      /*
       *    ARC SHAPE
       */
      ArcShape * arcS = dynamic_cast<ArcShape*>(s);
      if(arcS != NULL){
        painter.setPen(Qt::green);
        dxScreen = xToScreenX(arcS->getX1()) - arcS->getX1()  + dxInstance;
        dyScreen = yToScreenY(arcS->getY1()) - arcS->getY1() - arcS->getBoundinBox().getHeight()  - dyInstance;
        painter.drawArc(boxToScreenRect(arcS->getBoundinBox().translate(dxScreen,dyScreen)), arcS->getStart()*16, arcS->getSpan()*16);
      }

      /*
       *    ELLIPSE SHAPE
       */
      EllipseShape * ellipseS = dynamic_cast<EllipseShape*>(s);
      if(ellipseS != NULL){
        painter.setPen(Qt::green);
        dxScreen = xToScreenX(ellipseS->getX1()) - ellipseS->getX1()  + dxInstance;
        dyScreen = yToScreenY(ellipseS->getY1()) - ellipseS->getY1() - ellipseS->getBoundinBox().getHeight()  - dyInstance;
        painter.drawEllipse(boxToScreenRect(ellipseS->getBoundinBox().translate(dxScreen,dyScreen)));
      }
    }
  }

  /**
   * @brief Overload from the previeus method that takes a cell as an argument
   * this method takes an instance, the it calls the same method by pasing the
   * master cell
   * 
   * @param inst        istance to be drawed
   * @param painter     painter as REFERENCE 
   */
  void CellWidget::drawCells(const Instance* inst, QPainter& painter ){
    int dx = inst->getPosition().getX();
    int dy = inst->getPosition().getY();

    drawCells(inst->getMasterCell(), painter, dx, dy);
    QString instName = inst->getName().c_str();

    painter.setPen(Qt::white);
    painter.drawText(xToScreenX(dx), yToScreenY(dy+5), instName);
  }

  /**
   * @brief 
   * 
   * @param term    Term to be drawed
   * @param painter QPainter as REFERENCE
   * @param sz      size of term
   */
  void CellWidget::drawTerm(const Term* term, QPainter& painter, int sz){
    painter.setFont(QFont("Arial", TERM_TEXT_SZ));
    painter.setPen(Qt::white);
    Box termBox(0,0,sz,sz);

    string nameTermStr  = term->getName();
    int    x            = term->getPosition().getX();
    int    y            = term->getPosition().getY();
    Term::Direction dir = term->getDirection();
  
    int dxScreen = xToScreenX(x - (termBox.getWidth()/2));
    int dyScreen = yToScreenY(y + (termBox.getHeight()/2));
    painter.fillRect(boxToScreenRect(termBox.translate(dxScreen,dyScreen)),Qt::red);

    //Write Term shape name
    int strLen = strlen(nameTermStr.c_str());
    QString nameTermQs = nameTermStr.c_str();
    
    if(dir == Term::In){
      painter.drawText(termBox.getX1() - (TERM_TEXT_SZ * strLen), termBox.getY1(), nameTermQs);
    }else if(dir == Term::Out){
      painter.drawText(termBox.getX2(), termBox.getY1() - (TERM_TEXT_SZ/2), nameTermQs);
    }
  }

  /**
   * @brief This function paints de border square or bounding box of the cell
   * It calls getBoundingBox function to get the rectangle, then it calculates
   * the position in the scren.
   * 
   * @param cell    Curretn cell
   * @param painter 
   */
  void CellWidget::drawSide(const Cell* cell, QPainter& painter){
    Box sideBox = cell->getSymbol()->getBoundingBox(); 

    int dxScreen = xToScreenX(sideBox.getX1()) - sideBox.getX1();
    int dyScreen = yToScreenY(sideBox.getY1()) - sideBox.getY1() - sideBox.getHeight();

    drawCellName(cell, painter, sideBox.getX1(), sideBox.getY2());
    painter.drawRect(boxToScreenRect(sideBox.translate(dxScreen, dyScreen)));
  }

  /**
   * @brief Draw the cell name corresponding to the cell passed as argument
   * 
   * @param cell    Cell to draw name
   * @param painter Painter as REFERENCE
   * @param x       position of text X
   * @param y       Position of text Y
   */
  void CellWidget::drawCellName(const Cell* cell, QPainter& painter, int x, int y){
    
    // Text parameters
    const int fontSize = 24;
    painter.setFont(QFont("Arial", fontSize));
    painter.setPen(Qt::white);

    int x1, y1, len;

    
    //Draw text
    string cellName = cell->getName() + ".xml";
    QString cellNameQ = cellName.c_str();

    len = strlen(cellName.c_str()) / 2;
    x1 = (((screenRectToBox(rect()).getX2() - screenRectToBox(rect()).getX1()))/2) - ((len*fontSize)/2);
    y1 = fontSize;

    painter.drawText(x1, y1, cellNameQ);
  }

  /**
   * @brief This method draws all connection lines between nodes, Term componentns and
   * Terms from amin cell. The existent lines belong to a unique net. The lines
   * are stocked on a vector inside its nets. Every line has a node source and 
   * node Target.
   * 3 Types of nodes...
   *  1 - Node of a Term of an instance.
   *  2 - Node of a Term of the main cell
   *  3 - NodePoint. This is only a connection represented as a point in the diagram.
   *      It has no term associated
   * 
   * @param cell Main cell
   * @param painter Painter by REFERENCE
   */
  void CellWidget::drawLine(const Cell* cell, QPainter& painter){
    painter.setPen(Qt::gray);         //

    int x1, x2, y1, y2;
    int dxSource, dySource, dxTarget, dyTarget;

    for(auto *n : cell->getNets()){ // Iterate nets
      for(auto *l : n->getLines()){ // Iterate lines from each net
                
        //Reset delta variables each iteration
        dxSource = 0;
        dySource = 0;
        dxTarget = 0;
        dyTarget = 0;
                        
        /**
         * @brief This pattern of conditions an for loops searcha an calculates the cordinates for
         * soruce point. Each node type has different tratement.
         * Node 1 .- Term and Instance Associated: Read Instance Position and adds to it the TermShape Position of
         *              the master cell
         * Node 2 .- Term associated: Read The position of the term by comparing the name
         * Node 3 .- Node Point : Read the position of the point
         * 
         */
        if(l->getSource()->getTerm() != NULL){  // Node Type 1 or 2? -- Term associated
          if(l->getSource()->getTerm()->getInstance() != NULL){ // Node Type 1? - Node with a Instance associated
            // ***** NODE TYPE 1 - Term and instance associated to the node
            for(auto * i : cell->getInstances()){     // Iterate Instances
              for(auto * t : i->getTerms()){          // Iterate terms from instance
                if(t == l->getSource()->getTerm()){   // Search the instance by comparing each term
                  dxSource = i->getPosition().getX(); // Read Instance Position X
                  dySource = i->getPosition().getY(); // Read Instance Position Y
                  for(auto * s : i->getMasterCell()->getSymbol()->getShapes()){ //Iterate Shapes form master cell from instance
                    TermShape * termS = dynamic_cast<TermShape*>(s);
                    if(termS){ //Search TermShapes
                      if(termS->getTerm()->getName() == t->getName()){ //Search same term as current (t) by comparing names
                        dxSource += termS->getX1();   // Add TermShape Position to the instance position of x
                        dySource += termS->getY1();   // // Add TermShape Position to the instance position y
                        break;
                      }
                    }
                  }   
                  x1 = xToScreenX(dxSource);  //Calculate x cordiante for source X1
                  y1 = yToScreenY(dySource);  //Caluclate y cordiante for soruce y1
                  break;
                }

              }
            }      
          }else {// ***** NODE TYPE 2 - Term from main cell. No Instance Associated *****
            string termNameTmp;
            for(auto * t : cell->getTerms()){
              if(t == l->getSource()->getTerm()){
                termNameTmp = l->getSource()->getTerm()->getName();           // Get term Name
                dxSource = cell->getTerm(termNameTmp)->getPosition().getX();  // Get Term Position X fomr given term Name
                dySource = cell->getTerm(termNameTmp)->getPosition().getY();  // Get Term Position Y fomr given term Name
                x1 = xToScreenX(dxSource);  //Calculate x Cordinate for soruce x1
                y1 = yToScreenY(dySource);  //Calculate y Cordinate for soruce y1
                break;  
              }
            }
          }
        }else{ // ***** NODE TYPE 3 - No Term Associated. Only point in diagram *****
          x1 = xToScreenX(l->getSourcePosition().getX() + dxSource);
          y1 = yToScreenY(l->getSourcePosition().getY() + dySource);
        }

        //
        //  SAME PATTER AS BELOW, THIS TIME WITH TARGET NODE
        //
        if(l->getTarget()->getTerm() != NULL){  // Node with a Term associated?
          if(l->getTarget()->getTerm()->getInstance() != NULL){ // Node with a Instance associated?
            for(auto * i : cell->getInstances()){
              for(auto * t : i->getTerms()){
                if(t == l->getTarget()->getTerm()){
                  dxTarget = i->getPosition().getX();
                  dyTarget = i->getPosition().getY();
                  for(auto * s : i->getMasterCell()->getSymbol()->getShapes()){ //Iterate Shapes form master cell from instance
                    TermShape * termS = dynamic_cast<TermShape*>(s);
                    if(termS){ //Search TermShapes
                       if(termS->getTerm()->getName() == t->getName()){ //Search same term as current (t) by comparing names
                         dxTarget += termS->getX1();
                         dyTarget += termS->getY1();
                       }
                    }
                  }
                  x2 = xToScreenX(dxTarget);
                  y2 = yToScreenY(dyTarget);
                  break;
                }
              }
            }      
          }else { //  No Instance Associate, Terms Of the cell
            for(auto * t : cell->getTerms()){
              if(t == l->getTarget()->getTerm()){
                dxTarget = cell->getTerm(l->getTarget()->getTerm()->getName())->getPosition().getX();
                dyTarget = cell->getTerm(l->getTarget()->getTerm()->getName())->getPosition().getY();
                x2 = xToScreenX(dxTarget);
                y2 = yToScreenY(dyTarget);
                break;
              }
            }
          }
        }else{
          x2 = xToScreenX(l->getTargetPosition().getX() + dxTarget);
          y2 = yToScreenY(l->getTargetPosition().getY() + dyTarget);
        }
        
        //Draw line with calculated cordenates.
        painter.drawLine(x1, y1, x2, y2);
      }
    }
  }

  /**
   * @brief Draw NodePoints. Nodes that are not connected to a Term nor Instance.
   * 
   * @param cell Main cell
   * @param painter Painter by REFERENCE
   */
  void CellWidget::drawNodePoints(const Cell* cell, QPainter& painter){
    for(auto * net : cell->getNets()){
      for(auto * node : net->getNodes()){
        NodePoint * nodeP = dynamic_cast<NodePoint*>(node);
        if(nodeP){  //Search only NodePoints.
          QPoint p(xToScreenX(nodeP->getPosition().getX()), yToScreenY(nodeP->getPosition().getY()));
          painter.drawEllipse(p, 4, 4);
          painter.drawEllipse(p, 3, 3);
          painter.drawEllipse(p, 2, 2);
          painter.drawEllipse(p, 1, 1);
        }
      }
    }
  }

  void CellWidget::getScrValues() const {
        cout << "x1:" << viewport_.getX1() << " y1:" << viewport_.getY1() << " x2:" << viewport_.getX2() << " y2:" << viewport_.getY2() << endl;
  }


}  // Netlist namespace.