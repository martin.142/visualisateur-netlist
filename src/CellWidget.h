/**
 * @file CellWidget.h
 * @author ACOSTA CORRAL Martin Eduardo (martin.acostaec@gmail.com)
 * @author BOUHMID Zakaria              (zakaria.bmd@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2022-01-02
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef NETLIST_CELL_WIDGET_H
#define NETLIST_CELL_WIDGET_H

#include <QWidget>
#include <QPixmap>
#include <QPainter>
#include <QRect>
#include <QPoint>
#include "Box.h"
#include "Shape.h"


namespace Netlist {

  class Cell;
  class NodeTerm;


  class CellWidget : public QWidget {
      Q_OBJECT;
    public:
                      CellWidget         ( QWidget* parent=NULL );
      virtual        ~CellWidget         ();
      virtual QSize   minimumSizeHint    () const;
      virtual void    resizeEvent        ( QResizeEvent* );
              void    setCell            ( Cell* );
      inline  Cell*   getCell            () const;

      inline QRect    boxToScreenRect     (const Box&) const;
      inline QPoint   pointToScreenPoint  (const Point& )const;

      inline Box      screenRectToBox     (const QRect& ) const;
      inline Point    screenPointToPoint  (const QPoint& ) const;

      inline int      xToScreenX          (int x) const;
      inline int      yToScreenY          (int y) const;
      inline int      screenXToX          (int x) const;
      inline int      screenYToY          (int y) const;

             void     drawCells           (const Cell*, QPainter&, int dxInstance=0, int dyInstance=0, bool drawName=false); 
             void     drawCells           (const Instance*, QPainter&); 
             void     drawTerm            (const Term*, QPainter& painter,int sz=6);
             void     drawSide            (const Cell*, QPainter&);
             void     drawLine            (const Cell* cell, QPainter& painter);
             void     drawNodePoints      (const Cell* cell, QPainter& painter);
             void     drawCellName        (const Cell* cell, QPainter& painter, int x, int y);

             void     getScrValues        () const;
             void     cellPosition        ();


    protected:
      virtual void    paintEvent          (QPaintEvent*);
      virtual void    keyPressEvent       (QKeyEvent*);
      virtual void    mousePressEvent     (QMouseEvent *);

    signals:
              void    leftKey             ();
              void    rightKey            ();
              void    upKey               ();
              void    downKey             ();
    public slots:
            void      goLeft();
            void      goRight();
            void      goUp();
            void      goDown();
    private:
      Cell* cell_;
      Box   viewport_;
  };


  inline Cell* CellWidget::getCell () const { return cell_; }

  inline QRect CellWidget::boxToScreenRect(const Box& box) const{
    return  QRect(box.getX1(), box.getY1(), box.getWidth(), box.getHeight());
  }
  inline QPoint   CellWidget::pointToScreenPoint  (const Point& point)const{
    return  QPoint(point.getX(), point.getY());
  }
  inline Box      CellWidget::screenRectToBox     (const QRect& qRect) const{
    return Box(qRect.x(), qRect.y(), (qRect.x() + qRect.width()), (qRect.y() + qRect.height()));
  }
  inline Point    CellWidget::screenPointToPoint  (const QPoint& qPoint) const{
    return Point(qPoint.x(), qPoint.y());
  }

  inline int CellWidget::xToScreenX(int x) const { return x - viewport_.getX1(); } 
  inline int CellWidget::yToScreenY(int y) const { return viewport_.getY2() - y; }
  inline int CellWidget::screenXToX(int x) const { return x + viewport_.getX1(); }
  inline int CellWidget::screenYToY(int y) const { return viewport_.getY2() - y; } 


}  // Netlist namespace.

#endif  // NETLIST_CELL_WIDGET_H