/**
 * @file Node.cpp
 * @author ACOSTA CORRAL Martin Eduardo (martin.acostaec@gmail.com)
 * @author BOUHMID Zakaria              (zakaria.bmd@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2022-01-02
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <libxml/xmlreader.h>
#include <limits>
#include <sstream>
#include "Node.h" //TME7
#include "Line.h" //TME7
#include "Term.h" 
#include "Net.h" 
#include "Instance.h" //TME7
#include "Cell.h"
#include "XmlUtil.h"
#include "Point.h"


namespace Netlist {

  using namespace std;

  const size_t  Node::noid = numeric_limits<size_t>::max();

  /**
   * @brief Construct a new Node:: Node object
   * CONSTRUCTOR OVERLOADED
   * Term and id
   * 
   * @param term 
   * @param id 
   */
  Node::Node ( Term* term, size_t id )
    : id_      (id)
    , term_    (term)
    , position_()
    , lines_   ()
  {}

  /**
   * @brief Construct a new Node:: Node object
   * CONSTRUCTOR OVERLOADED
   * Only, id, no term associated
   * 
   * @param id 
   */
  Node::Node ( size_t id )
    : id_      (id)
    , term_(NULL) 
    , position_()
    , lines_   ()
  { }

  /**
   * @brief Construct a new Node:: Node object
   * CONSTRUCTOR OVERLOADED
   * Copy constructor
   * 
   * @param node 
   */
  Node::Node(const Node& node){
    id_       = node.id_;
    term_     = node.term_;
    position_ = node.position_;
  }

  /**
   * @brief Destroy the Node:: Node object
   * Deletes elements from vector of lines
   * 
   */
  Node::~Node(){
    while (not lines_.empty()){
      delete lines_[0];
    }
  }

  /**
   * @brief Add a new line to a node, only if the line isn't already
   * in the vector
   *  
   * @param line line to be added
   */
  void Node::attach (Line* line){
    for (size_t i = 0; i < lines_.size(); i++){
      if(lines_[i] == line)
        return;
      lines_.push_back(line);
    }
  }

  /**
   * @brief Remove a line from vector of lines.
   * 
   * @param line 
   */
  void Node::detach(Line* line){
    vector<Line*>::iterator iline = lines_.begin();
    for(; iline != lines_.end() ; ++iline){
      if((*iline) == line){
        lines_.erase( iline );
        break;
      }
    }
  }

  /**
   * @brief Construct a new Node Point:: Node Point object
   * Node poit is a heritd constructor class that created a node
   * that is not linked to a term nor instance, but only to a net
   * 
   * 
   * @param net   Net to be linkt to the node point
   * @param id    Id of the node
   * @param p     Position of the node
   */
  NodePoint::NodePoint (Net* net, size_t id, Point p): Node(id), net_(net){
    setPosition(p);
  }

  NodePoint::~NodePoint ()
  { net_->remove( this ); }

  Net* NodePoint::getNet () const
  { return net_; }

  /**
   * @brief write toXml data in the ostream
   * 
   * @param stream 
   */
  void  NodePoint::toXml ( ostream& stream ) const
  {
    stream << indent << "<node x=\""  << position_.getX()
                     <<    "\" y=\""  << position_.getY()
                     <<    "\" id=\"" << id_ << "\"/>\n";
  }

  NodeTerm::NodeTerm ( Term* term, size_t id )
    : Node(id), term_(term)
  { }


  NodeTerm::~NodeTerm ()
  { if (getNet()) getNet()->remove( this ); }


  Net* NodeTerm::getNet () const
  { return term_->getNet(); }


  /**
   * @brief write toXml data in the ostream
   * 
   * @param stream 
   */
  void  NodeTerm::toXml ( ostream& stream ) const
  {
    if (term_->isInternal()) {
      stream << indent << "<node term=\"" << term_->getName()
                       << "\" instance=\"" << term_->getInstance()->getName()
                       << "\" id=\""       << id_;
    } else {
      stream << indent << "<node term=\"" << term_->getName()
             << "\" id=\""       << id_;
    }
    stream << "\" x=\"" << position_.getX() << "\" y=\"" << position_.getY() << "\"/>\n";
  }

  Node& Node::operator=(const Node& node){
    this->id_       = node.id_;
    this->term_     = node.term_;
    this->position_ = node.position_;
    return *this;
  }
  
  /**
   * @brief This function could be declared as an inline function
   * but, as Node.h file uses forward declaration for term
   * the compilator is not able to see the full declaration of term.
   * 
   * @return Net* 
   */
  Net* Node::getNet() const { return term_->getNet(); }





  /*
   * ================================================================================================================
   * ================================================================================================================
   * ======================================                          ================================================
   * ====================================== READ AND WRITE XML FILES ================================================
   * ======================================                          ================================================
   * ================================================================================================================
   * ================================================================================================================
   */



  void Node::toXml(std::ostream& stream) const{
    if(getTerm()->isExternal()){
        stream  << indent << "<node term=\""  << getTerm()->getName() << "\"" 
                          << " id=\""         << getId()              << "\"/>\n";
    }else{
        stream  << indent << "<node term=\""  << getTerm()->getName()                 << "\"" 
                          << " instance=\""   << getTerm()->getInstance()->getName()  << "\"" 
                          << " id=\""         << getId()                              << "\"/>\n";
    }
  }


  /*
   * Read attributs from a xml fromXml element
   * fromXml Element does not creat a object node
   * it creates a connection betewen termes and nodes
   */
  bool Node::fromXml(Cell* cell, xmlTextReaderPtr& reader, Net* net){
    string termName     = xmlCharToString(xmlTextReaderGetAttribute( reader, (const xmlChar*)"term"     ) );
    string idStr        = xmlCharToString(xmlTextReaderGetAttribute( reader, (const xmlChar*)"id"       ) );
    string instanceName = xmlCharToString(xmlTextReaderGetAttribute( reader, (const xmlChar*)"instance" ) );

    int id = std::stoi(idStr);

    if(instanceName.empty()){
      if(termName.empty()){
        int x = std::stoi(xmlCharToString(xmlTextReaderGetAttribute(reader, (const xmlChar*)"x")));
        int y = std::stoi(xmlCharToString(xmlTextReaderGetAttribute(reader, (const xmlChar*)"y")));

        Point p(x, y);
        net->add(new NodePoint(net, id, p));
        return true;
      }else{
        cell->getTerm(termName)->getNode()->setId(id);
        net->add(cell->getTerm(termName)->getNode());
        return true;
      }
    } else {
      if(termName.empty())
        return false;

      cell->getInstance(instanceName)->getTerm(termName)->getNode()->setId(id);
      net->add(cell->getInstance(instanceName)->getTerm(termName)->getNode());
      return true;
    }

    
  }


}  // Netlist namespace.
