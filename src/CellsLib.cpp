/**
 * @file CellsLib.cpp
 * @author ACOSTA CORRAL Martin Eduardo (martin.acostaec@gmail.com)
 * @author BOUHMID Zakaria              (zakaria.bmd@gmail.com)
 * @brief function to operate the windows of cells
 * @version 0.1
 * @date 2022-01-02
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include  <QResizeEvent>
#include  <QPainter>
#include  <QPen>
#include  <QBrush>
#include  <QFont>
#include  <QApplication>
#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QDialog>
#include <QString>
#include <QHeaderView>
#include <QTableView>
#include <iostream>

#include "CellsLib.h"

namespace Netlist {

  using namespace std;

  CellsLib::CellsLib ( QWidget* parent )
    : QWidget(parent)
    , cellViewer_  ()
    , baseModel_ (new CellsModel(this))
    , view_ (new QTableView(this))
    , load_ (new QPushButton(this))
  {
    
    setAttribute( Qt::WA_QuitOnClose , false );
    setAttribute( Qt::WA_DeleteOnClose , false );
    setContextMenuPolicy( Qt:: ActionsContextMenu );

    /**
     * @brief set table attributes
     * 
     */
    view_ ->setShowGrid ( false );
    view_ ->setAlternatingRowColors( true );
    view_ ->setSelectionBehavior ( QAbstractItemView::SelectRows );
    view_ ->setSelectionMode ( QAbstractItemView::SingleSelection );
    view_ ->setSortingEnabled ( true );
    view_ ->setModel ( baseModel_ ); // On associe le model

    /**
     * @brief header of table attributes
     * Text alligned
     * size of 300
     * 
     */
    QHeaderView* horizontalHeader = view_ ->horizontalHeader ();
    horizontalHeader ->setDefaultAlignment ( Qt::AlignHCenter );
    horizontalHeader ->setMinimumSectionSize( 300 );
    horizontalHeader ->setStretchLastSection( true );

    QHeaderView* verticalHeader = view_ ->verticalHeader ();
    verticalHeader ->setVisible( false );


    /**
     * @brief HORIZONTAL LAYOUT
     * Strech = |
     * | Load button |
     * 
     */
    QHBoxLayout* hLayout = new QHBoxLayout ();
    hLayout->addStretch();
    hLayout->addWidget( load_ );
    hLayout->addStretch();


    /**
     * @brief Vertical layout
     * 
     *  TABLE OF CELL
     *  HORIZONTAL LAYOUT
     * 
     */
    QVBoxLayout* vLayout = new QVBoxLayout ();
    vLayout->addWidget( view_ );
    vLayout->addLayout( hLayout );
    setMinimumWidth(300);
    setMinimumHeight(500);
    setLayout ( vLayout );

    load_ ->setText( "Load" );
    connect( load_, SIGNAL(clicked ()), this, SLOT(load ()) );

    //Detect Double click on table view and call load()
    connect (view_, SIGNAL(doubleClicked(const QModelIndex &)), this, SLOT(load()));
  }

  CellsLib::~CellsLib ()
  { }

  /**
   * @brief Set cell selected when load button clicked or
   * when double click over the cell item in the list
   * 
   */
  void CellsLib::load()
  {
    int selectedRow = getSelectedRow ();
    if (selectedRow < 0) return;
    cellViewer_->setCell( baseModel_->getModel(selectedRow) );
  }

  /**
   * @brief Get number of the row selected
   * 
   * @return int index of row selected
   */
  int CellsLib::getSelectedRow () const
  {
    QModelIndexList selecteds = view_->selectionModel()->selection().indexes ();
    if (selecteds.empty ()) return -1;
    return selecteds.first().row();
  }

  void CellsLib::setCellViewer( CellViewer* cellViewer)
  {
    cellViewer_ = cellViewer;
  }

  CellsModel* CellsLib::getBaseModel(){
    return baseModel_;
  }

}