/**
 * @file Shape.cpp
 * @author ACOSTA CORRAL Martin Eduardo (martin.acostaec@gmail.com)
 * @author BOUHMID Zakaria              (zakaria.bmd@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2022-01-02
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <libxml/xmlreader.h>
#include <iostream>
#include "Shape.h"
#include "Symbol.h"
#include "XmlUtil.h"
#include "Cell.h"

namespace Netlist {

  /*
   * ==============================================================
   *  ---- MAIN SHAPE CLASS
   * ==============================================================
   */

  Shape::Shape  ( Symbol* owner ) 
    : owner_(owner)
  {  
    owner->add(this);
  }

  Shape::~Shape () { 
    owner_->remove( this ); 
  }

  void Shape::toXml(std::ostream& stream) const{
    //Not actions to do
  }

/**
 * @brief This method of the abstract class of shape only calls the correct
 * fromXml method of each herited class
 * 
 * @param owner 
 * @param reader 
 * @return Shape* Shape created
 */
Shape* Shape::fromXml ( Symbol* owner, xmlTextReaderPtr reader )
{
  // Factory-like method.
  const xmlChar* boxTag     = xmlTextReaderConstString( reader, (const xmlChar*)"box" );
  const xmlChar* ellipseTag = xmlTextReaderConstString( reader, (const xmlChar*)"ellipse" );
  const xmlChar* arcTag     = xmlTextReaderConstString( reader, (const xmlChar*)"arc" );
  const xmlChar* lineTag    = xmlTextReaderConstString( reader, (const xmlChar*)"line" );
  const xmlChar* termTag    = xmlTextReaderConstString( reader, (const xmlChar*)"term" );

  const xmlChar* nodeName   = xmlTextReaderConstLocalName( reader );

  Shape* shape = NULL;
  if (boxTag == nodeName)
    shape = BoxShape::fromXml( owner, reader );
  if (ellipseTag == nodeName)
    shape = EllipseShape::fromXml( owner, reader );
  if (arcTag == nodeName)
    shape = ArcShape::fromXml( owner, reader );
  if (lineTag == nodeName)
    shape = LineShape::fromXml( owner, reader );
  if (termTag == nodeName)
    shape = TermShape::fromXml( owner, reader );

  if (shape == NULL)
    std::cerr << "[ERROR] Unknown or misplaced tag <" << nodeName << "> (line:"
         << xmlTextReaderGetParserLineNumber(reader) << ")." << std::endl;

  return shape;
}


  /*
   * ==============================================================
   *  ---- BOX SHAPE
   * ==============================================================
   */

  BoxShape::BoxShape ( Symbol* owner, const Box& box)
    : Shape(owner), box_(box)
  {}

  BoxShape::BoxShape ( Symbol* owner, int x1, int y1, int x2, int y2)
    : Shape(owner), box_(x1, y1, x2, y2)
  {} 

  BoxShape::~BoxShape(){  }

  void BoxShape::toXml(std::ostream& stream) const{
    stream << indent << "<box x1=\"" << box_.getX1()
                  <<    "\" y1=\""   << box_.getY1()
                  <<    "\" x2=\""   << box_.getX2()
                  <<    "\" y2=\""   << box_.getY2() << "\"/>\n";
  }

  Shape* BoxShape::fromXml(Symbol* owner, xmlTextReaderPtr reader){
    int x1 = std::stoi(xmlCharToString(xmlTextReaderGetAttribute(reader, (const xmlChar*)"x1")));
    int y1 = std::stoi(xmlCharToString(xmlTextReaderGetAttribute(reader, (const xmlChar*)"y1"))); 
    int x2 = std::stoi(xmlCharToString(xmlTextReaderGetAttribute(reader, (const xmlChar*)"x2")));
    int y2 = std::stoi(xmlCharToString(xmlTextReaderGetAttribute(reader, (const xmlChar*)"y2"))); 

    BoxShape* temp = new BoxShape(owner, x1, y1, x2, y2);  // Create a temporal BoxShape to add and return;
    return temp;
  }


  /*
   * ==============================================================
   *  ---- LINE SHAPE
   * ==============================================================
   */

  LineShape::LineShape ( Symbol* owner, int x1, int y1, int x2, int y2 )
    : Shape(owner), x1_(x1), y1_(y1), x2_(x2), y2_(y2)
  {
  }

  LineShape::~LineShape(){  }

  void LineShape::toXml(std::ostream& stream) const{
    stream  << indent << "<box x1=\"" << x1_
            <<    "\" y1=\""          << y1_
            <<    "\" x2=\""          << x2_
            <<    "\" y2=\""          << y2_ << "\"/>\n";
  }

  Shape* LineShape::fromXml(Symbol* owner, xmlTextReaderPtr reader){
    int x1 = std::stoi(xmlCharToString(xmlTextReaderGetAttribute(reader, (const xmlChar*)"x1")));
    int y1 = std::stoi(xmlCharToString(xmlTextReaderGetAttribute(reader, (const xmlChar*)"y1"))); 
    int x2 = std::stoi(xmlCharToString(xmlTextReaderGetAttribute(reader, (const xmlChar*)"x2")));
    int y2 = std::stoi(xmlCharToString(xmlTextReaderGetAttribute(reader, (const xmlChar*)"y2"))); 

    LineShape* temp = new LineShape(owner, x1, y1, x2, y2);  // Create a temporal BoxShape to add and return;
    return temp;
  }

  /*
   * ==============================================================
   *  ---- TERM SHAPE
   * ==============================================================
   */
  TermShape::TermShape ( Symbol* owner, std::string name, int x1, int y1, NameAlign align)
    : Shape(owner), term_(NULL), x_(x1), y_(y1), align_(align)
  {
    term_ = owner->getCell()->getTerm(name);  
    if(term_->getNode())
      term_->getNode()->setPosition(x_, y_);
  }

  TermShape::~TermShape(){}

  void TermShape::toXml(std::ostream& stream) const{
    stream  << indent << "<term name=\"" << term_->getName()
            <<    "\" x1=\""             << x_
            <<    "\" y1=\""             << y_
            <<    "\" align=\""          << toString(align_) << "\"/>\n"; 
  }

  
  std::string TermShape::toString(NameAlign align){
  	std::string tab[4] = {"top_left","top_right", "bottom_left", "bottom_right"};
  	return tab[align-1];
  }

  /*
   * Compares a string with the name of Directions as String, it return the index
   * or 6 (unknow) if not found
   */
  TermShape::NameAlign TermShape::toAlign(std::string str){
  	std::string tab[4] = {"top_left","top_right", "bottom_left", "bottom_right"};
  	for(int i = 1; i<=4; i++){
  		if(!tab[i-1].compare(str)){
        return (NameAlign)i; //Cast constant integer to Direction enum number
  		}
  	}
  	return top_left; //Cast constant integer to Direction enum number
  }

  Shape* TermShape::fromXml(Symbol* owner, xmlTextReaderPtr reader){
    std::string TermName =           xmlCharToString(xmlTextReaderGetAttribute(reader, (const xmlChar*)"name" ) );
    std::string position =           xmlCharToString(xmlTextReaderGetAttribute(reader, (const xmlChar*)"align" ) );
    int         x1       = std::stoi(xmlCharToString(xmlTextReaderGetAttribute(reader, (const xmlChar*)"x1")));
    int         y1       = std::stoi(xmlCharToString(xmlTextReaderGetAttribute(reader, (const xmlChar*)"y1"))); 

    NameAlign alignTmp = toAlign(position);

    //Test If Valid Name (Not EMpty)
    if(TermName.empty()){
      std::cerr << "[ERROR] TermName empty in TermShape::fromXml()" << std::endl;
    }

    if(owner->getCell()->getTerm(TermName)->getPosition().getX() == 0 and owner->getCell()->getTerm(TermName)->getPosition().getY() == 0){
      //owner->getCell()->getTerm(TermName)->setPosition(x1, y1);       
    }
    TermShape* temp = new TermShape(owner, TermName,x1, y1, alignTmp);  // Create a temporal BoxShape to add and return;
    return temp;
  }


  /*
   * ==============================================================
   *  ---- ELLIPSE SHAPE
   * ==============================================================
   */

  EllipseShape::EllipseShape ( Symbol* owner, const Box& box)
    : Shape(owner), box_(box)
  {}

  EllipseShape::EllipseShape ( Symbol* owner, int x1, int y1, int x2, int y2)
    : Shape(owner), box_(x1, y1, x2, y2)
  {  
  } 

  EllipseShape::~EllipseShape(){  }

  void EllipseShape::toXml(std::ostream& stream) const{
    stream << indent << "<ellipse x1=\"" << box_.getX1()
                  <<    "\" y1=\""   << box_.getY1()
                  <<    "\" x2=\""   << box_.getX2()
                  <<    "\" y2=\""   << box_.getY2() << "\"/>\n";
  }

  Shape* EllipseShape::fromXml(Symbol* owner, xmlTextReaderPtr reader){
    int x1 = std::stoi(xmlCharToString(xmlTextReaderGetAttribute(reader, (const xmlChar*)"x1")));
    int y1 = std::stoi(xmlCharToString(xmlTextReaderGetAttribute(reader, (const xmlChar*)"y1"))); 
    int x2 = std::stoi(xmlCharToString(xmlTextReaderGetAttribute(reader, (const xmlChar*)"x2")));
    int y2 = std::stoi(xmlCharToString(xmlTextReaderGetAttribute(reader, (const xmlChar*)"y2"))); 

    EllipseShape* temp = new EllipseShape(owner, x1, y1, x2, y2);  // Create a temporal EllipseShape to add and return;
    return temp;
  }

  /*
   * ==============================================================
   *  ---- ARC SHAPE
   * ==============================================================
   */

  ArcShape::ArcShape ( Symbol* owner, const Box& box, int start, int span)
    : Shape(owner), box_(box), start_(start), span_(span)
  {}

  ArcShape::ArcShape ( Symbol* owner, int x1, int y1, int x2, int y2, int start, int span)
    : Shape(owner), box_(x1, y1, x2, y2), start_(start), span_(span)
  {  
  } 

  ArcShape::~ArcShape(){  }

  void ArcShape::toXml(std::ostream& stream) const{
    stream << indent << "<arc x1=\"" << box_.getX1()
                  <<    "\" y1=\""   << box_.getY1()
                  <<    "\" x2=\""   << box_.getX2()
                  <<    "\" y2=\""   << box_.getY2()
                  <<    "\" start=\""<< start_
                  <<    "\" span=\"" << span_ << "\"/>\n";
  }

  Shape* ArcShape::fromXml(Symbol* owner, xmlTextReaderPtr reader){
    int x1    = std::stoi(xmlCharToString(xmlTextReaderGetAttribute(reader, (const xmlChar*)"x1")));
    int y1    = std::stoi(xmlCharToString(xmlTextReaderGetAttribute(reader, (const xmlChar*)"y1"))); 
    int x2    = std::stoi(xmlCharToString(xmlTextReaderGetAttribute(reader, (const xmlChar*)"x2")));
    int y2    = std::stoi(xmlCharToString(xmlTextReaderGetAttribute(reader, (const xmlChar*)"y2"))); 
    int start = std::stoi(xmlCharToString(xmlTextReaderGetAttribute(reader, (const xmlChar*)"start")));
    int span  = std::stoi(xmlCharToString(xmlTextReaderGetAttribute(reader, (const xmlChar*)"span"))); 

    ArcShape* temp = new ArcShape(owner, x1, y1, x2, y2, start, span);  // Create a temporal ArcShape to add and return;
    return temp;
  }

  
} // namespace Netlist