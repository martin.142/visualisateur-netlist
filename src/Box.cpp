// -*- explicit-buffer-name: "Box.cpp<M1-MOBJ/7>" -*-

#include "Box.h"

namespace Netlist {


  Box& Box::merge ( const Box& other )
  {
    if (not other.isEmpty()) {
      if (isEmpty()) {
        x1_ = other.x1_;
        y1_ = other.y1_;
        x2_ = other.x2_;
        y2_ = other.y2_;
      } else {
        x1_ = std::min( x1_, other.x1_ );
        y1_ = std::min( y1_, other.y1_ );
        x2_ = std::max( x2_, other.x2_ );
        y2_ = std::max( y2_, other.y2_ );
      }
    }
    return *this;
  }

  bool  Box::intersection ( const Box& other ) const
  {
    if ( isEmpty() or other.isEmpty() ) return false;
    if (   ((x2_ < other.x1_) or (x1_ > other.x2_))
       and ((y2_ < other.y1_) or (y1_ > other.y2_)) ) return false;
    return true;
  }
  
  
  Box  Box::getIntersection ( const Box& other )
  {
    if (not intersection(other) ) return Box();
    return Box ( std::max(x1_,other.x1_)
               , std::max(y1_,other.y1_)
               , std::min(x2_,other.x2_)
               , std::min(y2_,other.y2_)
               );
  }
  
  
  Box& Box::inflate ( int dx1, int dy1, int dx2, int dy2 )
  {
    x1_ -= dx1;
    y1_ -= dy1;
    x2_ += dx2;
    y2_ += dy2;
  
    return *this;
  }
  
  
  std::ostream&  operator<< ( std::ostream& o, const Box& box )
  {
    o << "<Box " << box.x1_ << " " << box.y1_ << " " << box.x2_ << " " << box.y2_ << "/>";
    return o;
  }


}  // Netlist namespace.