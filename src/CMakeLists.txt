# -*- explicit-buffer-name: "CMakeLists.txt<M1-MOBJ/8-10>" -*-
#
# To display executed commands, use:
#     > cmake -DCMAKE_VERBOSE_MAKEFILE:STRING=YES ../src

cmake_minimum_required(VERSION 2.8.0)
project(SCHEMATIC)

set (CMAKE_CXX_FLAGS            "-Wall -g -std=c++11" CACHE STRING "C++ Compiler Release options." FORCE)
set (CMAKE_INSTALL_PREFIX       "../install" )

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)

if(CMAKE_VERSION VERSION_LESS "3.7.0")
    set(CMAKE_INCLUDE_CURRENT_DIR ON)
endif()

find_package(LibXml2 REQUIRED)
find_package(Qt5 COMPONENTS Widgets REQUIRED)

    # Trouver les includes de LibXml2.
   include_directories( ${SCHEMATIC_SOURCE_DIR} 
			 ${LIBXML2_INCLUDE_DIR} )

                    set( includes     Indentation.h
                                      XmlUtil.h 
                                      Point.h
                                      Box.h
                                      Term.h
                                      Net.h
                                      Instance.h
                                      Node.h
                                      Line.h
                                      Shape.h
                                      Symbol.h
                                      Cell.h
                       )

                    set( mocIncludes  SaveCellDialog.h
                                      OpenCellDialog.h
                                      InstancesWidget.h
                                      InstancesModel.h
                                      CellsLib.h
                                      CellsModel.h
                                      CellWidget.h
                                      CellViewer.h
                       )

                    set( cpps         Indentation.cpp
                                      XmlUtil.cpp 
                                      Point.cpp
                                      Box.cpp
                                      Term.cpp
                                      Net.cpp
                                      Instance.cpp
                                      Node.cpp
                                      Line.cpp
                                      Shape.cpp
                                      Symbol.cpp
                                      Cell.cpp
                                      SaveCellDialog.cpp
                                      OpenCellDialog.cpp
                                      InstancesWidget.cpp
                                      InstancesModel.cpp
                                      CellsLib.cpp
                                      CellsModel.cpp
                                      CellWidget.cpp
                                      CellViewer.cpp
                                      Main.cpp
                       )

           qt5_wrap_cpp( mocCpps      ${mocIncludes} )

         add_executable( tme810       ${cpps} ${mocCpps} )
  target_link_libraries( tme810       Qt5::Widgets ${LIBXML2_LIBRARIES} )
                install( TARGETS      tme810                      DESTINATION bin )
                install( FILES        ${includes} ${mocIncludes}  DESTINATION include )
