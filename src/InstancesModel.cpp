/**
 * @file InstancesModel.cpp
 * @author ACOSTA CORRAL Martin Eduardo (martin.acostaec@gmail.com)
 * @author BOUHMID Zakaria              (zakaria.bmd@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2022-01-02
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <QString>
#include "InstancesModel.h"
#include  "Net.h"
#include  "Instance.h"
#include <iostream>

#include "Cell.h"


namespace Netlist {

  using namespace std;

  InstancesModel::InstancesModel( QObject* parent )
    : QAbstractTableModel(parent)
    , cell_( NULL )
  {
    
  }

  InstancesModel::~InstancesModel() {}

  /**
   * @brief Set cell will change data from the model
   * the method stop the "refreshing" event that is executed by QT, teng changes de data 
   * ten re-start the refreshing of the data
   * 
   * @param cell Cell to be set as main one
   */
  void  InstancesModel::setCell( Cell* cell )
  {
    emit layoutAboutToBeChanged();
    cell_ = cell;
    emit layoutChanged();
  }

  /**
   * @brief OVERRIDED FUNCTION
   * Nuber of row to be created depending on the size of vector of istances
   * 
   * @param parent 
   * @return int  Nomber of rows
   */
  int InstancesModel::rowCount(const QModelIndex &parent) const 
  {
    return (cell_) ? cell_->getInstances().size() : 0;
  }

  /**
   * @brief Number of columns to be created
   * in this case is a constant value, so 2 columbs will be showed
   * 
   * @param parent 
   * @return int 
   */
  int InstancesModel::columnCount(const QModelIndex &parent) const
  {
    return 2;
  }

  /**
   * @brief Function executed by QT, it iterates the vector of instances
   * for case 0: Data to be showed in column 0 : name of instance
   * for case 1: data to be showed in column 1 : Name of master cell
   * 
   * @param index     Row to be readed
   * @param role      
   * @return QVariant Variant is a generic datatype
   */
  QVariant InstancesModel::data(const QModelIndex &index, int role) const
  {
    if (not cell_ or not index.isValid ()) return QVariant ();
    if (role == Qt:: DisplayRole) {
      int row = index.row();
      switch( index.column() ) {
        case 0: return cell_->getInstances()[row]->getName().c_str();
        case 1: return cell_->getInstances()[row]->getMasterCell()->getName().c_str();
      }
    }
    return QVariant ();
  }

  /**
   * @brief Headers data.
   * as this table is a two column, a name for each header have to be assigned
   * 
   * @param section     
   * @param orientation 
   * @param role 
   * @return QVariant 
   */
  QVariant InstancesModel::headerData(int section, Qt::Orientation orientation, int role) const
  {
    if (orientation == Qt:: Vertical) return QVariant ();
    if (role != Qt:: DisplayRole) return QVariant ();
    switch ( section ) {
      case 0: return "Instance";
      case 1: return "MasterCell";
    }
    return QVariant ();
  }

  /**
   * @brief Read cell sellected in the table
   * 
   * @param row 
   * @return Cell* 
   */
  Cell* InstancesModel::getModel ( int row )
  {
    if (not cell_) return NULL;
    if ( row >= (int)cell_->getInstances().size() ) return NULL;
    return cell_ ->getInstances()[row]->getMasterCell();
  }

}  // Netlist namespace.