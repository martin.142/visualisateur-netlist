// -*- explicit-buffer-name: "Main.cpp<M1-MOBJ/7>" -*-

#include <iostream>
#include <QApplication>
#include <QtGui>
#include "Cell.h"
#include "CellViewer.h"
#include "Term.h"

using namespace Netlist;
using namespace std;

int main(int argc, char* argv[]){

   cout << "Chargement des modeles..." << endl;
  Cell* vdd = Cell::load( "vdd" );
  Cell::load( "gnd" );
  Cell::load( "TransistorN" );
  Cell::load( "TransistorP" );
  Cell::load( "and2" );
  Cell::load( "or2" );
  Cell::load( "xor2" );
  
  QApplication* qa = new QApplication(argc, argv);
  CellViewer* viewer = new CellViewer();
  viewer->setCell(vdd);
  viewer->show();

  int rvalue = qa->exec();
  delete qa;
  return rvalue;
}