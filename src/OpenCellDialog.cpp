/**
 * @file OpenCellDialog.cpp
 * @author ACOSTA CORRAL Martin Eduardo (martin.acostaec@gmail.com)
 * @author BOUHMID Zakaria              (zakaria.bmd@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2022-01-02
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QDialog>
#include "OpenCellDialog.h"

namespace Netlist
{
  OpenCellDialog::OpenCellDialog ( QWidget * parent )
  : QDialog ( parent )
  , lineEdit_ ( NULL )
  {
    /**
     * @brief Object of the window
     * Title
     * Label = Enter cell Name
     * Line edit to enter the cell to be opend
     * Ok button
     * Cancel button
     * 
     */
    setWindowTitle ( tr ( " Open Cell ") );
    QLabel * label = new QLabel ();
    label -> setText ( tr (" Enter Cell name ") );
    lineEdit_ = new QLineEdit ();
    lineEdit_ -> setMinimumWidth ( 400 );
    QPushButton * okButton = new QPushButton ();
    okButton -> setText ( " OK " );
    okButton -> setDefault ( true );
    QPushButton * cancelButton = new QPushButton ();
    cancelButton -> setText ( " Cancel " );

    /**
     * @brief Layout arangement
     * Horisontal layout. Strech = |
     *  | OK | Cancel |
     * 
     * Vertical layout
     *  Label
     *  LineEdit
     *  Horizontal Layout
     * 
     * 
     * Connect Signals (button actions) with accept or reject()
     */
    QHBoxLayout * hLayout = new QHBoxLayout ();
    hLayout -> addStretch ();
    hLayout -> addWidget ( okButton );
    hLayout -> addStretch ();
    hLayout -> addWidget ( cancelButton );
    hLayout -> addStretch ();
    QFrame * separator = new QFrame ();
    separator -> setFrameShape ( QFrame :: HLine );
    separator -> setFrameShadow ( QFrame :: Sunken );
    QVBoxLayout * vLayout = new QVBoxLayout ();
    vLayout -> setSizeConstraint ( QLayout :: SetFixedSize );
    vLayout -> addWidget ( label );
    vLayout -> addWidget ( lineEdit_ );
    vLayout -> addLayout ( hLayout );
    setLayout ( vLayout );

    connect ( okButton , SIGNAL ( clicked ()) , this , SLOT ( accept ()) );
    connect ( cancelButton , SIGNAL ( clicked ()) , this , SLOT ( reject ()) );
  }

  /**
   * @brief Read Line edit
   * 
   * @return const QString 
   */
  const QString OpenCellDialog::getCellName () const
  { return lineEdit_->text (); }

  /**
   * @brief Set Line Edit
   * 
   * @param name 
   */
  void OpenCellDialog::setCellName ( const QString & name )
  { return lineEdit_->setText ( name ); }
  
  /**
   * @brief Wait for click on OK or CANCEL button
   * 
   * @param name  NOT USED
   * @return true   OK button clicked
   * @return false  CANCEL button clicked
   */
  bool OpenCellDialog::run ( QString & name )
  {
    setCellName ( name );
    int dialogResult = exec();
    name = getCellName ();
    return ( dialogResult == Accepted );
  }
  
} // namespace Netlist
