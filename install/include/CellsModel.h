/**
 * @file CellsModel.h
 * @author ACOSTA CORRAL Martin Eduardo (martin.acostaec@gmail.com)
 * @author BOUHMID Zakaria              (zakaria.bmd@gmail.com)
 * @brief Model data for  cell list
 * @version 0.1
 * @date 2022-01-02
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef NETLIST_CELLS_MODEL_H
#define NETLIST_CELLS_MODEL_H

#include <QAbstractTableModel>
#include <vector>

namespace Netlist {

  class Cell;

  class CellsModel : public QAbstractTableModel {
      Q_OBJECT;
    public:
                        CellsModel    ( QObject* parent=NULL );
      virtual          ~CellsModel    ();
              Cell*     getModel      ( int row );
              int       rowCount      (const QModelIndex &parent = QModelIndex()) const override;
              int       columnCount   (const QModelIndex &parent = QModelIndex()) const override;
              QVariant  data          (const QModelIndex &index, int role = Qt::DisplayRole) const override;
              QVariant  headerData    (int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
              
      public slots:
              void      updateDatas   ();

    private:
      std::vector<Cell*> cells_;
  };


}  // Netlist namespace.

#endif  // NETLIST_CELLS_MODEL_H