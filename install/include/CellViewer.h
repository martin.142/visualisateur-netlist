/**
 * @file CellViewer.h
 * @author ACOSTA CORRAL Martin Eduardo (martin.acostaec@gmail.com)
 * @author BOUHMID Zakaria              (zakaria.bmd@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2022-01-02
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef NETLIST_CELL_CELLVIEWER_H
#define NETLIST_CELL_CELLVIEWER_H

#include <QMainWindow>
#include <QWidget>

namespace Netlist {

  class Cell;
  class CellWidget;
  class CellsLib;
  class SaveCellDialog;
  class OpenCellDialog;
  class InstancesWidget;

  class CellViewer : public QMainWindow {
      Q_OBJECT;
    public:
                        CellViewer          ( QWidget* parent=NULL );
      virtual          ~CellViewer          ();
              Cell*     getCell             () const;
      inline  CellsLib* getCellsLib         ();  
              void      setCell             ( Cell* );
    public slots: 
              void      saveCell            ();
              void      openCell            ();
              void      showCellsLib        ();  
              void      showInstancesWidget ();   
    signals:  
              void      cellLoaded();
    private:
      CellWidget*      cellWidget_;
      CellsLib*        cellsLib_;         
      InstancesWidget* instancesWidget_;  
      SaveCellDialog*  saveCellDialog_;
      OpenCellDialog*  openCellDialog_;
  };

}

#endif //NETLIST_CELL_CELLVIEWER_H