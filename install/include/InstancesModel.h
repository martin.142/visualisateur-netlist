/**
 * @file InstancesModel.h
 * @author ACOSTA CORRAL Martin Eduardo (martin.acostaec@gmail.com)
 * @author BOUHMID Zakaria              (zakaria.bmd@gmail.com)
 * @brief Model of Instances list
 * Function to read and set data int he list of instances
 * @version 0.1
 * @date 2022-01-02
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef NETLIST_INSTANCES_MODEL_H
#define NETLIST_INSTANCES_MODEL_H

#include <QAbstractTableModel>

//#include "Box.h"


namespace Netlist {

  class Cell;
  class CellViewer;
  class InstancesModel;


  class InstancesModel : public QAbstractTableModel {
      Q_OBJECT;
    public:
                        InstancesModel  ( QObject* parent=NULL );
      virtual          ~InstancesModel  ();
              void      setCell         (Cell*);
              Cell*     getModel        ( int row );
              int       rowCount        (const QModelIndex &parent = QModelIndex()) const override;
              int       columnCount     (const QModelIndex &parent = QModelIndex()) const override;
              QVariant  data            (const QModelIndex &index, int role = Qt::DisplayRole) const override;
              QVariant  headerData      (int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
              
    private:
      Cell* cell_;
  };


}  // Netlist namespace.

#endif  // NETLIST_INSTANCES_MODEL_H