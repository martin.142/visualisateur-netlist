/**
 * @file Symbol.h
 * @author ACOSTA CORRAL Martin Eduardo (martin.acostaec@gmail.com)
 * @author BOUHMID Zakaria              (zakaria.bmd@gmail.com)
 * @brief Symbol associated to a Cell, it is te symbol used when a cell 
 * has no instances or a cell is an instance of another cell
 * @version 0.1
 * @date 2022-01-02
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef NETLIST_SYMBOL_H
#define NETLIST_SYMBOL_H

  #include  <libxml/xmlreader.h>
  #include  <string>
  #include  <vector>
  #include  "Box.h"

namespace Netlist {

  class Term;
  class Cell;
  class Shape;
  class TermShape;


  class Symbol {
    public:
                                        Symbol          ( Cell* );
                                       ~Symbol          ();
      inline        Cell*               getCell         () const;
                    Box                 getBoundingBox  () const;
                    Point               getTermPosition ( Term* ) const;
                    TermShape*          getTermShape    ( Term* ) const;
      inline const  std::vector<Shape*> getShapes       () const;
                    void                add             ( Shape* );
                    void                remove          ( Shape* );
                    void                toXml           ( std::ostream& ) const;
                    static Symbol*      fromXml         ( Cell*, xmlTextReaderPtr );
    private:                                            
                                        Symbol          ( const Symbol& );
             Symbol&                    operator=       ( const Symbol& );
    private:
      Cell*                owner_;
      std::vector<Shape*>  shapes_;
  };


  inline const  std::vector<Shape*>   Symbol::getShapes      () const { return shapes_; }
  inline        Cell*                 Symbol::getCell        () const {return owner_;}

}  // Netlist namespace.

#endif  // NETLIST_SYMBOL_H