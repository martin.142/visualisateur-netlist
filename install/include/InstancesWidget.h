/**
 * @file InstancesWidget.h
 * @author ACOSTA CORRAL Martin Eduardo (martin.acostaec@gmail.com)
 * @author BOUHMID Zakaria              (zakaria.bmd@gmail.com)
 * @brief QWidget that show the list of instances of the current cell
 * @version 0.1
 * @date 2022-01-02
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef NETLIST_INSTANCES_WIDGET_H
#define NETLIST_INSTANCES_WIDGET_H

#include <QWidget>
#include <QPixmap>
#include <QPainter>
#include <QRect>
#include <QPoint>
#include <QTableView>
#include <QPushButton>
#include <iostream>
#include "CellViewer.h"
#include "InstancesModel.h"


namespace Netlist {

  class Cell;


  class InstancesWidget : public QWidget {
      Q_OBJECT;
    public:
                      InstancesWidget     ( QWidget* parent=NULL );
      virtual        ~InstancesWidget     ();
              void    setCellViewer       ( CellViewer* );
              int     getSelectedRow      () const;
              void    setCell             ( Cell* );

    public slots:
              void    load                ();

    private:
      CellViewer*     cellViewer_;    // Main cell viewer
      InstancesModel* baseModel_;     // Model of instances data
      QTableView*     view_;          // QTable view for listing instances
      QPushButton*    load_;          // Button load
  };



}  // Netlist namespace.

#endif  // NETLIST_INSTANCES_WIDGET_H