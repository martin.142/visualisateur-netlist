/**
 * @file Node.h
 * @author ACOSTA CORRAL Martin Eduardo (martin.acostaec@gmail.com)
 * @author BOUHMID Zakaria              (zakaria.bmd@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2022-01-02
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef NETLIST_NODE_H
#define NETLIST_NODE_H
#include  <libxml/xmlreader.h>
#include <iostream>
#include <vector>


#include "Indentation.h"
#include "Point.h"

namespace Netlist {

  class Term;
  class Net;
  class Node;
  class Cell;
  class Line;

  class Node {
    public:
      static const size_t  noid;
    public:
                      Node        ( Term*, size_t id=noid );
                      Node        (size_t id);
                     ~Node        ();

      inline  size_t  getId       () const;
      inline  Term*   getTerm     () const;
      inline  Point   getPosition () const;
              Net*    getNet      () const;

      inline  void    setId       ( size_t );
      inline  void    setPosition ( const Point& );
      inline  void    setPosition ( int x, int y );

      virtual void    toXml       (std::ostream& stream) const;

      static  bool    fromXml     (Cell*, xmlTextReaderPtr&,Net*);

      // ********* TME7
      inline  const   std::vector<Line*>& getLines  () const;
      inline          size_t              getDegree () const;
                      void                attach    (Line*);
                      void                detach    (Line*);

    private:
                      Node        ( const Node& );
              Node&   operator=   ( const Node& );
    protected:
      size_t  id_;
      Term*   term_;
      Point   position_;
      std::vector<Line*> lines_;
  };

  inline size_t  Node::getId       () const { return id_; }
  inline Term*   Node::getTerm     () const { return term_; }
  inline Point   Node::getPosition () const { return position_; }
  inline void    Node::setId       ( size_t id ) { id_=id; }
  inline void    Node::setPosition ( const Point& pos ) { position_ = pos; }
  inline void    Node::setPosition ( int x, int y )     { setPosition(Point(x,y)); }

  // ************  TME 7
  inline const std::vector<Line*>& Node::getLines () const {return lines_;}
  inline       size_t              Node::getDegree() const {return lines_.size();}



  /*
   * CLASS NodePoint et NodeTerm TME7
   */
  class NodePoint : public Node {
    public:
                         NodePoint   ( Net*, size_t id=Node::noid, Point p=Point() );
      virtual           ~NodePoint   ();
      virtual Net*       getNet      () const;
              void       toXml       ( std::ostream& ) const;
    private:
                         NodePoint   ( const NodePoint& );
              NodePoint& operator=   ( const NodePoint& );
    private:
      Net* net_;
  };


  class NodeTerm : public Node {
    public:
                         NodeTerm    ( Term*, size_t id=Node::noid );
      virtual           ~NodeTerm    ();
      inline   Term*     getTerm     () const;
      virtual  Net*      getNet      () const;
      virtual  void      toXml       ( std::ostream& ) const;
    private:
                         NodeTerm    ( const NodeTerm& );
               NodeTerm& operator=   ( const NodeTerm& );
    private:
      Term* term_;
  };


  inline Term* NodeTerm::getTerm () const { return term_; }


}  // Netlist namespace.

#endif  // NETLIST_NODE_H
