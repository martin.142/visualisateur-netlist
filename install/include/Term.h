/**
 * @file Term.h
 * @author ACOSTA CORRAL Martin Eduardo (martin.acostaec@gmail.com)
 * @author BOUHMID Zakaria              (zakaria.bmd@gmail.com)
 * @brief Term class
 * A term is a connection in the cell, it could be part of the cell or part of an instance
 * @version 0.1
 * @date 2022-01-02
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef  NETLIST_TERM_H   
#define  NETLIST_TERM_H   

#include  <libxml/xmlreader.h>
#include <iostream>
#include "Node.h"
#include "Instance.h"
#include "Point.h"

namespace Netlist{
	
	class Net;
	class Cell;
	
	class Term{
		public:
			enum Type				{Internal = 1, External = 2};
			enum Direction 	{In=1, Out=2, Inout=3, Tristate=4, Transcv=5, Unknown=6 };

			static std::string  			toString    	( Type );
			static std::string  			toString    	( Direction );
			static Direction    			toDirection 	( std::string );	

															  Term 					( Cell*    ,  std::string, Direction dir );
															  Term 					( Instance*, const Term* modelTerm );
															  ~Term 				();
															  
			inline  bool              isInternal   	() const;
			inline  bool              isExternal   	() const;
			inline  std::string 		 	getName      	() const;
			inline  Node*             getNode      	();
			inline	Net*              getNet       	() const;
			inline  Cell*             getCell      	() const;
			inline  Cell*             getOwnerCell 	() const;
			inline  Instance*         getInstance  	() const;
			inline  Direction         getDirection 	() const;
			inline  Point             getPosition  	() const;
			inline  Type              getType      	() const;

			inline 	void  						setDirection 	( Direction );
							void  						setNet       	( Net* );
			       	void  						setNet       	( std::string );
			       	void  						setPosition  	( const Point );
			       	void  						setPosition  	( int x, int y );

			       	void							toXml				 	(std::ostream& stream);

			static  Term*					 		fromXml				(Cell*, xmlTextReaderPtr);

		private:
			void*         owner_;				// Instance or Cell
			std::string   name_; 				
			Direction     direction_;		// In, out, inout, tristate, transcv, unknow
			Term::Type    type_;				// Internal, external
			Net*          net_;					// Pointer to Net associated to this ter
			Node          node_; 				// Object Node in this term
			Point  				position_;

	};

	inline bool 						Term::isInternal		() const{return type_ == Internal;}
	inline bool 						Term::isExternal		() const{return type_ == External;}
	inline std::string 			Term::getName				() const{return name_;}
	inline Node*						Term::getNode				() 			{return &node_;}
	inline Cell* 						Term::getCell 			() const{return (type_ == External)? static_cast<Cell*>(owner_) : nullptr;}
	inline Cell* 						Term::getOwnerCell 	() const{return (type_ == Internal)? static_cast<Instance*>(owner_)->getCell() : static_cast<Cell*>(owner_);}
	inline Instance* 				Term::getInstance 	() const{return (type_ == Internal)? static_cast<Instance*>(owner_): nullptr;}
	inline Term::Direction 	Term::getDirection	() const{return direction_;}
	inline Point 						Term::getPosition		() const{return position_;}
  inline Term::Type 			Term::getType				() const{return type_;}
  inline Net* 						Term::getNet 				() const{return net_;}


	inline void 						Term::setDirection 	(Direction dir){direction_ = dir;}

} // namespace Netlist

#endif