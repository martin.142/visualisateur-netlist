/**
 * @file Shape.h
 * @author ACOSTA CORRAL Martin Eduardo (martin.acostaec@gmail.com)
 * @author BOUHMID Zakaria              (zakaria.bmd@gmail.com)
 * @brief Shape is an abstract class that heritates to other shape as
 * Box, Line, Term, Arc and Ellipse. 
 * @version 0.1
 * @date 2022-01-02
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef NETLIST_SHAPE_H
#define NETLIST_SHAPE_H

#include  <libxml/xmlreader.h>
#include "Box.h"
#include "Line.h"
#include "Term.h"


namespace Netlist {

  class Symbol;

  class Shape {
  public:    

                        Shape         ( Symbol* );
    virtual             ~Shape        ();
    inline  Symbol*     getSymbol     () const;
    virtual Box         getBoundinBox () const = 0;
    virtual void        toXml         (std::ostream& stream) const;
    
    static  Shape*      fromXml       (Symbol* owner, xmlTextReaderPtr reader);
    
    private:
    Symbol*   owner_;
  };

  inline Symbol* Shape::getSymbol() const { return owner_; }
  



  /*
   *==============================================================
   *  ---- BOX SHAPE
   * ==============================================================
   */

  class BoxShape : public Shape {
    public:
                    BoxShape      ( Symbol*, const Box&);
                    BoxShape      ( Symbol*, int x1, int y1, int x2, int y2);
                    ~BoxShape     ();
             Box    getBoundinBox () const override {return box_;} 
      inline int    getX1         ()const;
      inline int    getX2         ()const;
      inline int    getY1         ()const;
      inline int    getY2         ()const;
             void   toXml         (std::ostream& stream) const; 

      static Shape* fromXml(Symbol* owner, xmlTextReaderPtr reader);
    private:
      Box box_;
  };

   //Box BoxShape::getBoundinBox() const { return box_;}
  inline int BoxShape::getX1()const{return box_.getX1();}
  inline int BoxShape::getY1()const{return box_.getY1();}
  inline int BoxShape::getX2()const{return box_.getX2();}
  inline int BoxShape::getY2()const{return box_.getY2();}




  
  /*
   * ==============================================================
   *  ---- LINE SHAPE
   * ==============================================================
   */
  class LineShape : public Shape {
    public:
            LineShape        ( Symbol*, int x1, int y1, int x2, int y2);
            ~LineShape       ();
             Box   getBoundinBox  () const override {return Box(x1_, y1_, x2_, y2_); }  
      inline int   getX1()const;
      inline int   getX2()const;
      inline int   getY1()const;
      inline int   getY2()const;
      void  toXml(std::ostream& stream) const;

      static Shape* fromXml(Symbol* owner, xmlTextReaderPtr reader); 

    private:
      int x1_, y1_, x2_, y2_;
  };

         //Box LineShape::getBoundinBox() const {return Box(x1_, y1_, x2_, y2_); }
  inline int LineShape::getX1()const{return x1_;}
  inline int LineShape::getX2()const{return x2_;}
  inline int LineShape::getY1()const{return y1_;}
  inline int LineShape::getY2()const{return y2_;}






  /*
   * ==============================================================
   *  ---- TERM SHAPE
   * ==============================================================
   */
  class TermShape : public Shape {
    public:
    enum NameAlign { top_left=1, top_right, bottom_left, bottom_right }; //----------------- TME7
    
    static std::string  toString ( NameAlign );
		static NameAlign    toAlign  ( std::string );


                    TermShape         ( Symbol*, std::string name, int x, int y, NameAlign);
                    ~TermShape        ();
              Box   getBoundinBox     () const override {return Box(x_, y_, x_, y_);}
      inline  Term* getTerm           () const;
      inline  int   getX1              () const;
      inline  int   getY1              () const;
      inline  NameAlign getNameAlign  () const;
              void  toXml             (std::ostream& stream) const; 

      static  Shape* fromXml(Symbol* owner, xmlTextReaderPtr reader);

    private:
      Term*           term_;
      int             x_, y_;
      NameAlign       align_;
  };

  inline Term*  TermShape::getTerm  () const {return term_;}
  inline int    TermShape::getX1     () const {return x_;}
  inline int    TermShape::getY1     () const {return y_;}
  inline TermShape::NameAlign TermShape::getNameAlign  () const { return align_;}






  /*
   * ==============================================================
   *  ---- ELLIPSE SHAPE
   * ==============================================================
   */
  class EllipseShape : public Shape {
    public:
                    EllipseShape  ( Symbol*, const Box&);
                    EllipseShape  ( Symbol*, int x1, int y1, int x2, int y2);
                    ~EllipseShape ();
              Box   getBoundinBox () const override {return box_;} 
      inline  int    getX1        ()const;
      inline  int    getX2        ()const;
      inline  int    getY1        ()const;
      inline  int    getY2        ()const;
              void   toXml        (std::ostream& stream) const; 

      static Shape* fromXml       (Symbol* owner, xmlTextReaderPtr reader);

    private:
      Box box_;
  };

  inline int EllipseShape::getX1()const{return box_.getX1();}
  inline int EllipseShape::getY1()const{return box_.getY1();}
  inline int EllipseShape::getX2()const{return box_.getX2();}
  inline int EllipseShape::getY2()const{return box_.getY2();}







  /*
   * ==============================================================
   *  ---- ARC SHAPE
   * ==============================================================
   */
  class ArcShape : public Shape {
    public:
                    ArcShape     ( Symbol*, const Box&, int start, int span);
                    ArcShape     ( Symbol*, int x1, int y1, int x2, int y2, int start, int span);
                    ~ArcShape    ();
              Box   getBoundinBox()const override {return box_;} 
      inline  int   getStart     ()const;
      inline  int   getSpan      ()const;
      inline  int   getX1        ()const;
      inline  int   getX2        ()const;
      inline  int   getY1        ()const;
      inline  int   getY2        ()const;
              void  toXml(std::ostream& stream) const; 

      static  Shape* fromXml(Symbol* owner, xmlTextReaderPtr reader);

    private:
      Box box_;
      int start_;
      int span_;
  };

  inline int ArcShape::getStart ()const{return start_;}
  inline int ArcShape::getSpan  ()const{return span_;}
  inline int ArcShape::getX1    ()const{return box_.getX1();}
  inline int ArcShape::getY1    ()const{return box_.getY1();}
  inline int ArcShape::getX2    ()const{return box_.getX2();}
  inline int ArcShape::getY2    ()const{return box_.getY2();}

}

#endif