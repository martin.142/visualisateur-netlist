/**
 * @file Instance.h
 * @author ACOSTA CORRAL Martin Eduardo (martin.acostaec@gmail.com)
 * @author BOUHMID Zakaria              (zakaria.bmd@gmail.com)
 * @brief Instance of a cell in another cell
 * 
 * @version 0.1
 * @date 2022-01-02
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef  NETLIST_INSTANCE_H   
#define  NETLIST_INSTANCE_H   
#include  <libxml/xmlreader.h>
#include "Point.h"

namespace Netlist{

	class Term;
	class Cell;

	class Instance{
	public:

	 																	  Instance     ( Cell* owner, Cell* masterCell, std::string );
	 																	 ~Instance     ();

		inline const	std::vector<Term*>& getTerms      () 							const;
		inline 				std::string         getName       () 							const;
		inline 				Cell*               getMasterCell () 							const;
		inline 				Cell*               getCell       () 							const;
		inline 				Point               getPosition   () 							const;
									Term*               getTerm       (std::string ) 	const;

									bool  							connect       ( std::string name, Net* );
									void  							add           ( Term* );
									void  							remove        ( Term* );
									void  							setPosition   ( const Point& );
									void  							setPosition   ( int x, int y );
									
									void 							 	toXml				  (std::ostream& stream);
		static        Instance*					 	fromXml				(Cell*, xmlTextReaderPtr);

	private:
			Cell*                owner_;			// owner cell
			Cell*                masterCell_; // master cell
			std::string          name_;				// Name of INSTANCE
			std::vector<Term*>	 terms_;			// list of ters as type internal
			Point                position_;		// Posotion of the instance in the cell
	};

	/*
	 *  Inline functions
	 */
	inline const 	std::vector<Term*>& Instance::getTerms			() const{return terms_;}
	inline 				std::string 				Instance::getName				() const{return name_;}
	inline 				Cell* 							Instance::getMasterCell	() const{return masterCell_;}
	inline 				Cell* 							Instance::getCell 			() const{return owner_;}
	inline 				Point 							Instance::getPosition 	() const{return position_;}


}

#endif